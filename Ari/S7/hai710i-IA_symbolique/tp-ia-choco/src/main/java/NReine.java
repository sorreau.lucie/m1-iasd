import org.chocosolver.solver.Model;

import org.chocosolver.solver.variables.IntVar;


public class NReine {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Model model = new Model("Reine");
		
		int nbR=8 ;
		
		
		
		
		IntVar [] Reine = model.intVarArray("R",nbR,0,nbR-1);
		
		//check
		model.allDifferent(Reine).post();
		
		
		
		//check diagonal
		
		for (int i = 0; i < nbR; i++) {
			for (int j = i+1; j < nbR; j++) {
				model.arithm(Reine[i],"-",Reine[j],"!=", j-i).post();
				model.arithm(Reine[i],"-",Reine[j],"!=", i-j).post();

				
			}
			
		}
		
	       // Affichage du réseau de contraintes créé
        System.out.println("*** Réseau Initial ***");
        System.out.println(model);
        

        // Calcul de la première solution
        if(model.getSolver().solve()) {
        	System.out.println("\n\n*** Première solution ***");        
        	System.out.println(model);
        }
		
                
    	// Calcul de toutes les solutions
    	System.out.println("\n\n*** Autres solutions ***");        
        while(model.getSolver().solve()) {    	
            System.out.println("Sol "+ model.getSolver().getSolutionCount()+"\n"+model);
	    }
    	
		
		
		// crée un tableau de 5 variables entières de domaine [1,25]
		//IntVar [] t = model.intVarArray("x",5,1,25);
		

        
        // Affichage de l'ensemble des caractéristiques de résolution
      	System.out.println("\n\n*** Bilan ***");        
        model.getSolver().printStatistics();
        
	}

}
