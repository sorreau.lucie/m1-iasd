package dict;

public interface IDictionary {
	
	
	 Object get(Object key);
	 IDictionary put(Object key, Object val );
	 boolean isEmpty();
	 boolean containKey(Object key);
	 int size();
	

}
