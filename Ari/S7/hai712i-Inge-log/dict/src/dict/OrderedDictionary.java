package dict;

public class OrderedDictionary extends AbstractDictionary {
	
	
		public OrderedDictionary() {
			this.keys=new Object[0];
			this.def=new Object[0];
		}
	
	@Override
	protected   int indexOf(Object key) {
		for (int i = 0; i < keys.length; i++) {
			if(this.keys[i]==key) {
				return i;
			}
		}
		return -1;
	}

	@Override
	protected int newIndexOf(Object key) {
		// TODO Auto-generated method stub
		//int a=indexOf(key);  verifier
	 Object[] newKeys = new Object[keys.length+1];
	 Object[] newDef = new Object[def.length+1];
	 for (int i = 0; i < keys.length; i++) {
		 newKeys[i]=this.keys[i];
		 newDef[i]=this.def[i];
		
	}
	 this.keys=newKeys;
	 this.def=newDef;
	 return (keys.length-1);
		
	}
	
	
	
	
}
