
public class Forfait extends Compte {
	
	private Compte compte;

	
	
	
	

	public Forfait(Client client, Compte compte) {
		super(client);
		this.compte = compte;
	}

	public Forfait(Compte compte) {
		this.compte=compte;
		// TODO Auto-generated constructor stub
	}

	public Forfait(Client client) {
		super(client);
		// TODO Auto-generated constructor stub
	}

	public Client getClient() {
		return this.compte.getClient();
	}

	public double prixLocation(Produit p) {
		return compte.prixLocation(p);
	}
}
