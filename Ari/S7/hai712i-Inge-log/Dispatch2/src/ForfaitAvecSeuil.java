
public class ForfaitAvecSeuil extends Forfait {

	
	private final int seuil =2;
	private int compteur=0;

	public ForfaitAvecSeuil( Compte compte) {
		super(compte);
		// TODO Auto-generated constructor stub
	}
	public double prixLocation(Produit p) {
		if(this.compteur==seuil) {
			compteur=0;
			return 0;
		}else {
			compteur+=1;
			return p.prixLocation();
		}
	}
}
	
	

