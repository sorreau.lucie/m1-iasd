
public class CompteAvecSeuil extends Compte{
	
	private final int seuil=2;
	private int compteur=0;
	
	public CompteAvecSeuil(Client client) {
		super(client);
	}
	
	public double prixLocation(Produit p) {
		if(this.compteur==seuil) {
			compteur=0;
			return 0;
		}else {
			compteur+=1;
			return p.prixLocation();
		}
		
		
	}
	
	
	
	
	
	
	
	
	

}