
public class Compte  {

	

	private Client client;

	/**
	 * @param client
	 */
	public Compte(Client client) {
		this.client = client;
	}
	

	public Compte() {
		super();
	}


	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
	
	public double prixLocation(Produit p) {
		 return p.prixLocation();
		
	}
	



}

