prompt "Suppression des relations"
BEGIN
EXECUTE IMMEDIATE 'DROP TABLE coutServeur';
EXCEPTION
 WHEN OTHERS THEN
	IF SQLCODE != -942 THEN
	RAISE;
	END IF;
END;
/

BEGIN
EXECUTE IMMEDIATE 'DROP TABLE Jeux';
EXCEPTION
 WHEN OTHERS THEN
	IF SQLCODE != -942 THEN
	RAISE;
	END IF;
END;
/

BEGIN
EXECUTE IMMEDIATE 'DROP TABLE Maintenance';
EXCEPTION
 WHEN OTHERS THEN
	IF SQLCODE != -942 THEN
	RAISE;
	END IF;
END;
/
BEGIN
EXECUTE IMMEDIATE 'DROP TABLE Technicien';
EXCEPTION
 WHEN OTHERS THEN
	IF SQLCODE != -942 THEN
	RAISE;
	END IF;
END;
/
create table Technicien(
    idTechnicien int NOT NULL,
    entreprise varchar(50),
    nom varchar(50),
    prenom varchar(50),
    qualification varchar(50),
    dateNaissance DATE,
    salaire numeric(5,2),
    pays varchar(50),
    ville varchar(50),
    zoneAffectation varchar(50),
    equipe varchar(50),
    nomResponsable varchar(50),
    constraint pk_Technicien primary key(idTechnicien),
    constraint checkEquipe Check (equipe in ('matin', 'apres-midi', 'nuit', 'journee'))
);
create table Jeux(
    idJeux int NOT NULL,
    nom varchar(50),
    categorie varchar(50),
    nomDevelopper varchar(50),
    nomDistributeur varchar(50),
    dateSortie DATE,
    poids int,
    typeCharge varchar(50),
    constraint pk_Jeux primary key(idJeux),
    constraint checkTypeCharge Check (typeCharge in ('Lobby','Telechargement','Monde','Autre')) 
);


create table Maintenance(
    idMaintenance int NOT NULL,
    dateDebutMaintenance DATE,
    dateFinMaintenance DATE,
    equipeResponsable varchar(50),
    raisonMaintenance varchar(50),
    constraint pk_Maintenance primary key(idMaintenance), 
    constraint checkEquipeResponsable Check (equipeResponsable in ('matin','apres-midi', 'nuit', 'journee'))
);
create view dateServeur AS
Select *
FROM Dates;
create view TempsServeur AS
Select *
FROM Temps;

create view UtilisateurServeur AS
Select *
FROM Utilisateur;
create view ServeurServeur AS
SELECT *
From Serveur;

create table coutServeur(
    idTechnicien int,
    idJeux int,
    idDate int,
    idTemps int,
    idUtilisateur int,
    idMaintenance int,
    idServeur int,
    chargeTotalServeur numeric(38,2),
    prixTotalServeur numeric(38,2),
    prixMaintenance numeric(38,2),
    prixMemoire numeric(38,2),
    constraint pk_Serveur2 primary key (idTechnicien, idJeux, idDate, idTemps, idUtilisateur, idMaintenance),
    constraint fk_Technicien foreign key (idTechnicien) references Technicien(idTechnicien),
    constraint fk_idJeux foreign key (idJeux) references Jeux(idJeux),
    constraint fk_idDateSer foreign key (idDate) references Dates(idDate),
    constraint fk_idTempsSer foreign key (idTemps) references Temps(idTemps),
    constraint fk_idUtilisateurSer foreign key (idUtilisateur) references Utilisateur(idUtilisateur),
    constraint fk_idMaintenance foreign key (idMaintenance) references Maintenance(idMaintenance),
    constraint fk_idServeurSer foreign key (idServeur) references Serveur(idServeur)
);
