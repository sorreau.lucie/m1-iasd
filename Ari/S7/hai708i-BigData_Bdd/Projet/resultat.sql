Prompt----------Pour chaque jeux le nombre de vente moyenne par année ?----------;
SELECT nom,sum(quantite)/count(annee) as totVent,annee
FROM Objet
join ventes on objet.idObjet=Ventes.idObjet
join dates on ventes.idDate=Dates.idDate
Group by nom,annee
Order by  annee DESC;


Prompt----------Quels sont les distributeurs de jeux les plus populaires ?----------;

SELECT nomDevelopper, SUM(quantite)
FROM objet, Ventes
WHERE objet.idobjet=Ventes.idobjet
Group by nomDevelopper
Order by SUM(quantite) DESC;

Prompt----------Quels sont les régions où les utilisateurs achètent le plus ?----------;

SELECT region , sum(quantite) as Quantite
FROM Utilisateur
join Ventes on utilisateur.idUtilisateur=Ventes.idUtilisateur
Group by region
Order by Quantite DESC;

Prompt----------Quels est le nombre de joueurs par régions ?----------;

SELECT count(Utilisateur.idUtilisateur) as nbJoueurs,Serveur.region
FROM Utilisateur
join Ventes on Utilisateur.idUtilisateur=Ventes.idUtilisateur
join Serveur on Ventes.idServeur=Serveur.idServeur
Group by Serveur.region
Order by nbJoueurs DESC;

Prompt----------Quelle est le profit generée pour chaque Promotion ?----------;
SELECT nomPromotion,sum(profit) as Profit
FROM Ventes
join Promotion on Ventes.idPromotion=Promotion.idPromotion
Group by nomPromotion
Order by Profit DESC;
Prompt----------Quels sont les jeux ou il y a le plus de pannes serveurs ?----------;
SELECT Jeux.nom,count(idMaintenance) as nbTotalMaintenances
FROM Jeux
join coutServeur on Jeux.idJeux=coutServeur.idJeux
Group by Jeux.nom
Order by nbTotalMaintenances DESC;

Prompt----------Quel type de d'intervention le plus nécessaire pour le maintient des serveurs ?----------;
SELECT raisonMaintenance,count(*) as nbInterventions
FROM Maintenance
join coutServeur on Maintenance.idMaintenance=coutServeur.idMaintenance
Group by raisonMaintenance
Order by nbInterventions DESC;

Prompt----------Pour chaque jeux quel est le coût de maintien des serveurs ?----------;
SELECT Jeux.nom, SUM(coutServeur.prixMaintenance+coutServeur.prixMemoire) as prixTotal
FROM Jeux
join coutServeur on Jeux.idJeux=coutServeur.idJeux
Group by nom
Order by prixTotal DESC;

Prompt----------Quelles sont les régions avec le plus de pannes ?----------;
SELECT region,count(*) as nbPannes
FROM coutServeur
join Maintenance on coutServeur.idMaintenance=Maintenance.idMaintenance
join Serveur on coutServeur.idServeur=Serveur.idServeur
Group by region
Order by nbPannes DESC;

Prompt----------Quelles sont les qualifications plus demandées ?----------;
SELECT qualification,count(*) as nbTechnicien
FROM Technicien
join coutServeur on Technicien.idTechnicien=coutServeur.idTechnicien
Group by qualification
Order by nbTechnicien DESC;






