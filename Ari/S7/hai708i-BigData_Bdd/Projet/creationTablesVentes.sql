prompt "Suppression des relations"
BEGIN
EXECUTE IMMEDIATE 'DROP TABLE Ventes';
EXCEPTION
 WHEN OTHERS THEN
	IF SQLCODE != -942 THEN
	RAISE;
	END IF;
END;
/

BEGIN
EXECUTE IMMEDIATE 'DROP TABLE Utilisateur';
EXCEPTION
 WHEN OTHERS THEN
	IF SQLCODE != -942 THEN
	RAISE;
	END IF;
END;
/
BEGIN
EXECUTE IMMEDIATE 'DROP TABLE Serveur';
EXCEPTION
 WHEN OTHERS THEN
	IF SQLCODE != -942 THEN
	RAISE;
	END IF;
END;
/
BEGIN
EXECUTE IMMEDIATE 'DROP TABLE Promotion';
EXCEPTION
 WHEN OTHERS THEN
	IF SQLCODE != -942 THEN
	RAISE;
	END IF;
END;
/

BEGIN
EXECUTE IMMEDIATE 'DROP TABLE Objet';
EXCEPTION
 WHEN OTHERS THEN
	IF SQLCODE != -942 THEN
	RAISE;
	END IF;
END;
/

BEGIN
EXECUTE IMMEDIATE 'DROP TABLE Dates';
EXCEPTION
 WHEN OTHERS THEN
	IF SQLCODE != -942 THEN
	RAISE;
	END IF;
END;
/
BEGIN
EXECUTE IMMEDIATE 'DROP TABLE Temps';
EXCEPTION
 WHEN OTHERS THEN
	IF SQLCODE != -942 THEN
	RAISE;
	END IF;
END;
/



create TABLE Objet(
    idObjet int  NOT NULL,
    nom varchar(50),
    prix int,
    categoriePremier varchar(50),
    categorieSecondaire varchar(50),
    likes int,
    dislikes int,
    nomDevelopper varchar(50),
    nomPublisher varchar(50),
    releaseDate DATE,
    pourcentageVente real,
    constraint pk_Objet primary key (idObjet),
    constraint checkPourcent2 check (pourcentageVente between 0.0 and 1.0)  
);

create table Utilisateur(
    idUtilisateur int NOT NULL,
    pseudo varchar(50),
    nom varchar(50),
    niveau int,
    dateNaissance DATE,
    dateCreationCompte DATE,
    nbBanned int,
    adresseMail varchar(50),
    adressIP varchar(50),
    region varchar(50),
    genre varchar(50),
    montantPortefeuille numeric(38,2),
    constraint pk_Utilisateur primary key(idUtilisateur),
    constraint checkGenreServeur Check (genre in ('homme','femme','autre'))
);

create TABLE Serveur(
    idServeur int NOT NULL,
    nom varchar(50),
    region varchar(50),
    espaceTotal int,
    espaceLibre int,
    prixAchat int,
    prixLocation int,
    espaceLoue int,
    dateDebutLocation DATE,
    dateMisEnService DATE,
    constraint Pk_serveur primary key (idserveur)
);
create TABLE Promotion(
    idPromotion INT NOT NULL ,
    nomPromotion varchar(50),
    regionPromotion varchar(50),
    typeReduction varchar(50),
    valeurPromotion real,
    dateDebut DATE,
    dateFin Date,
    constraint pk_Promotion primary key (idPromotion),
    constraint checkTypeReduction Check (typeReduction in ('Net','Pourcent'))
 );

create TABLE Dates(
    idDate INT  NOT NULL ,
    dates DATE,
    jour int,
    mois int,
    annee int,
    numJourSemaine int,
    numJourMois int,
    numJourAnnee int,
    numeroTrimestre int,
    numeroSemestre int,
    vacances varchar(50),
    constraint pk_Dates primary key (idDate)
);

create TABLE Temps(
    idTemps INT NOT NULL ,
    heure int,
    minute int,
    seconde int,
    constraint pk_Temps primary key (idTemps)
);

create TABLE Ventes(
    idUtilisateur int,
    idObjet int,
    idPromotion int,
    idTemps int,
    idDate int,
    idServeur int,
    prixTotal numeric(38,2),
    quantite int,
    profit numeric(38,2),
    prixUnitaire numeric(38,2),
    POS int,
constraint pk_pkVentes Primary Key(idUtilisateur,idObjet,idPromotion,idTemps,idDate,idServeur),
    constraint fk_idUtilisateur foreign key (idUtilisateur) references Utilisateur(idUtilisateur), 
    constraint fk_idObjet foreign key (idObjet) references Objet(idObjet),
    constraint fk_idPromotion foreign key (idPromotion) references Promotion(idPromotion),
    constraint fk_idTempsV foreign key (idTemps) references Temps(idTemps),
    constraint fk_idDateV foreign key (idDate) references Dates(idDate),
    constraint fk_idServeur foreign key (idServeur) references Serveur(idServeur)
);

