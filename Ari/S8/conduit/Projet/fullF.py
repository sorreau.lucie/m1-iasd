import cv2
import numpy as np
from keras.models import model_from_json

# Define dictionary for emojis
emoji_dict = {"Angry": "angry.png", "Disgusted": "disgusted.png", "Fearful": "fearful.png",
              "Happy": "happy.png", "Neutral": "neutral.png", "Sad": "sad.png",
              "Surprised": "surprised.png"}
# Define dictionary for emotions
emotion_dict = {0: "Angry", 1: "Disgusted", 2: "Fearful", 3: "Happy", 4: "Neutral", 5: "Sad", 6: "Surprised"}

# Load model from disk
json_file = open('model/emotion_model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
emotion_model = model_from_json(loaded_model_json)
emotion_model.load_weights("model/emotion_model.h5")
print("Loaded model from disk")

# Initialize the camera
cap = cv2.VideoCapture(0)

while True:
    # Capture a frame
    ret, frame = cap.read()
    if not ret:
        break
        
    # Preprocess the image and predict the emotion
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    face_detector = cv2.CascadeClassifier('haarcascades/haarcascade_frontalface_default.xml')
    num_faces = face_detector.detectMultiScale(gray_frame, scaleFactor=1.3, minNeighbors=5)
    
    for (x, y, w, h) in num_faces:
        cv2.rectangle(frame, (x, y-50), (x+w, y+h+10), (0, 255, 0), 4)
        roi_gray_frame = gray_frame[y:y + h, x:x + w]
        cropped_img = np.expand_dims(np.expand_dims(cv2.resize(roi_gray_frame, (48, 48)), -1), 0)
        emotion_prediction = emotion_model.predict(cropped_img)
        maxindex = int(np.argmax(emotion_prediction))
        


        # Load the corresponding emoji image
        
        emoji_image = cv2.imread(f"emojis/{emoji_dict[emotion_dict[maxindex]]}")
        emoji_image = cv2.resize(emoji_image, (h, w))
        
         
        #emoji_image = cv2.imread(f"emojis/{emoji_dict[emotion_dict[maxindex]]}")
        #emoji_image = cv2.resize(emoji_image, (100, 100))
        x_offset = 10
        y_offset = 10 
        #frame[y_offset:y_offset+emoji_image.shape[0], x_offset:x_offset+emoji_image.shape[1]] = cv2.addWeighted(frame[y_offset:y_offset+emoji_image.shape[0], x_offset:x_offset+emoji_image.shape[1]], 0.7, emoji_image, 0.3, 0)
        #text
        #cv2.putText(frame, emotion_dict[maxindex], (x+5, y-20), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)
         
        
         #Overlay the emoji image onto the frame
        frame[y:y+h, x:x+w] = cv2.addWeighted(frame[y:y+h, x:x+w], 0.7, emoji_image, 0.3, 0)

    cv2.imshow('Emotion Detection', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
