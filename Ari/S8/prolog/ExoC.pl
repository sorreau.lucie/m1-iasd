appartient(X,[X|_]).
appartient(X,[_|L]) :-appartient(X,L).


non_appartient(X,L) :-appartient(X,L), !,fail.
non_appartient(_,_).


sans_repetition([_]).
sans_repetition([X|L]) :-non_appartient(X,L),sans_repetition(L).


ajout_tete(X,L1,[T|Q]) :- Q==L1,T==X.

ajout_que(X,[],[X]).
ajout_que(X,[T|Q],[H|L]) :- T==H, ajout_que(X,Q,L).

suprrimer(X,L1,L2):-L1==L2,non_appartient(X,L1).
suprrimer(X,[T1|Q1],L2):- T1==X,Q1==L2.
suprrimer(X,[T1|Q1],[T2|Q2]):- T1==T2,suprrimer(X,Q1,Q2).

suprrimer_fin(L1,L2):-L1==L2,L2==[].
suprrimer_fin([T1|Q1],[T2|Q2]):-T1==T2, suprrimer_fin(Q1,Q2).
suprrimer_fin([_],[]).

%TODO check if necessaireles les cas de bases 
fusion([],[],[]).
fusion([], L, L).
fusion(L, [], L).
fusion([X|L1], [Y|L2], [X,Y|L3]) :- fusion(L1, L2, L3).
fusion([X|L1], L2, [X|L3]) :- fusion(L1, L2, L3).
fusion(L1, [Y|L2], [Y|L3]) :- fusion(L1, L2, L3).

%elimine jsuqua L1 empty
concatener([], L2, L2).
concatener([T1|Q1], L2, [T1|Q3]) :- concatener(Q1, L2, Q3).

%123 321
%23 32 321 123 321
inverser([],[]).
inverser([X|L1],L2):-inverser(L1,L3),ajout_fin(X,L3,L2). 


commun([],L2,[]).
commun([X|L1],L2,L3):- appartient(X,L2),commun(L1,L2,L4),ajout_tete(X,L4,L3).
commun([X|L1],L2,L3):- non_appartient(X,L2),commun(L1,L2,L3).
%et linverse

ens([],[]).
ens([X|L1],L2):-appartient(X,L1),ens(L1,L2).
ens([X|L1],L2):-non_appartient(X,L1),ens(L1,L3),ajout_tete(X,L3,L2).
 


% L1 L2  L3  reunion 
reunion([],L2,L2).
reunion([X|L1],L2,L4):-appartient(X,L2),reunion(L1,L2,L4).
%L3 cacik oldu.
reunion([X|L1],L2,L3):-non_appartient(X,L2),  ajout_tete(X,L4,L3),reunion(L1,L2,L4).


