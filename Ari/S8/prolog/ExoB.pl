homme(albert).
homme(jean).
homme(paul).
homme(bertrand).
homme(louis).
homme(benoit).
homme(edgar).
femme(germaine).
femme(christiane).
femme(simone).
femme(marie).
femme(sophie).
femme(madeleine).
parent(albert,jean).
parent(jean,paul).
parent(paul,bertrand).
parent(paul,sophie).
parent(jean,simone).
parent(louis,benoit).
parent(paul,edgar).
parent(germaine,jean).
parent(christiane,simone).
parent(christiane,paul).
parent(simone,benoit).
parent(marie,bertrand).
parent(marie,sophie).
parent(madeleine,edgar).
mere(X,Y) :-parent(X,Y),femme(X).
pere(X,Y) :-parent(X,Y),homme(X).
grand_pere(X,Y) :-pere(X,P),parent(P,Y).
grand_pere_maternel(X,Y):- pere(X,Z),parent(Z,Y),femme(Z).
demi_frere(X,Y):-homme(X),parent(Z,X),parent(Z,Y),X\==Y.
oncle(X,Y):-homme(X),demi_frere(X,Z),mere(Z,Y).
oncle(X,Y):-homme(X),demi_frere(X,Z),pere(Z,Y).
ancetre(X,Y):- parent(X,Y).
ancetre(X,Y):- parent(X,I),ancetre(I,Y).
descendant(X,Y):- parent(Y,X).
descendant(X,Y):- parent(Z,X),ancetre(Z,Y).
