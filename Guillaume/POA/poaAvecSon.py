import random
import re



def createApt():
    global hauteur, largeur
    hauteur = int(input("Saisir la hauteur de l'appartement désiré \t"))
    largeur = int(input("Saisir la largeur de l'appartement désiré \t"))
    Apt = []

    print("on va créer l'appartement \n")
    print(" - Les obstacles sont représentés par un x  \n")
    print(" - Les portes d'entrées sont représentés par po si ouverte p sinon \n")
    print(" - Les fenêtres sont représentés par fo si ouverte f sinon \n")
    print(" - Les espace vides sont représentés par une valeur vide (entrée) \n")
    print(" - Le robot sera représenté par un @")
    inputPermis = ["x", "", "p", "f", "po", "fo", "s", "g"]
    for i in range(hauteur):
        print("saisir la ligne ", i, "contenant ", largeur, "valeurs\n")
        r = []
        for j in range(largeur):
            a = input()
            while (a not in inputPermis):
                print(" Cet input n'est pas valable, veuillez saisir à nouveau la valeur ", j, "\n")
                a = input()
            r.append(a)
        Apt.append(r)
    return Apt


def afficherApt(apt):
    for i in range(len(apt)):
        print(apt[i], "\n")
    print('\n')


def placementRobot(apt, nomRobot):
    i = 0
    while (i != 1):
        print("Veuillez choisir l'endroit ou vous souhaitez placer le robot \n")
        print("position x du robot\n")
        x = int(input())
        if x in range(0, len(apt)):
            print("position y du robot\n")
            y = int(input())
            if y in range(0, len(apt[1])):
                if apt[x][y] != "x" and "@" not in apt[x][y]:
                    i = 1

                    apt[x][y] = nomRobot + apt[x][y]
        if (i == 0):
            print("position invalide\n")


def positionRobotsinit(Apt):
    cordRobots = []
    # travers apt
    for i in range(hauteur):
        for j in range(largeur):
            # check chaque string, le robot commence avec @
            if (Apt[i][j].startswith("@", 0, 1)):
                nom = getNomRobot(Apt, i, j)
                # ajout les cord robots dans le liste
                # premier robot est a cord i,j
                cordRobots.append([nom, i, j, 0])  # dernier deplacement init a 0,avant 1 il a bougé vers le haut, 2
    return cordRobots


def checkDroite(Apt, i, j):  # i,j emplacement du robot
    return j + 1 <= largeur - 1 and "x" not in Apt[i][j + 1] and "@" not in Apt[i][j + 1]


def checkGauche(Apt, i, j):
    return j - 1 >= 0 and "x" not in Apt[i][j - 1] and "@" not in Apt[i][j - 1]


def checkHaut(Apt, i, j):
    return i - 1 >= 0 and "x" not in Apt[i - 1][j] and "@" not in Apt[i - 1][j]


def checkBas(Apt, i, j):
    return i + 1 <= hauteur - 1 and "x" not in Apt[i + 1][j] and "@" not in Apt[i + 1][j]


def checkFenetre(Apt, i, j):
    return ('fo' in Apt[i][j])


def FermeFenetre(Apt, i, j):
    nom = getNomRobot(Apt, i, j)
    Apt[i][j] = nom + 'f'
    print(nom, "ferme la fenêtre\n")


def FermePorte(Apt, i, j):
    nom = getNomRobot(Apt, i, j)
    Apt[i][j] = nom + 'p'
    print(nom, " ferme la porte\n")


def checkPorte(apt, i, j):
    return ('po' in apt[i][j])


def goDroite(Apt, i, j):
    nom = getNomRobot(Apt, i, j)
    SupprRobotCase(apt, i, j)
    Apt[i][j + 1] = nom + Apt[i][j + 1]
    print(nom, " va à droite\n")


def goGauche(Apt, i, j):
    nom = getNomRobot(Apt, i, j)
    SupprRobotCase(Apt, i, j)
    Apt[i][j - 1] = nom + Apt[i][j - 1]
    print(nom, " va à gauche\n")


def goHaut(Apt, i, j):
    nom = getNomRobot(Apt, i, j)
    SupprRobotCase(Apt, i, j)
    Apt[i - 1][j] = nom + Apt[i - 1][j]
    print(nom, " va en haut\n")


def goBas(Apt, i, j):
    nom = getNomRobot(Apt, i, j)
    SupprRobotCase(Apt, i, j)
    Apt[i + 1][j] = nom + Apt[i + 1][j]
    print(nom, " va en bas\n")


def getNomRobot(Apt, i, j):
    nomRobot = re.findall("^@[0-9]*", apt[i][j])
    return nomRobot[0]


def SupprRobotCase(Apt, i, j):
    Apt[i][j] = re.sub("^@[0-9]*", "", Apt[i][j])


def setNombreRobot(nombreRobot):
    print("combien de robot souhaitez vous ajouter ?\n")
    x = int(input())
    nb = nombreRobot
    # !! on ne fais pas de vérif sur le nombre de robot par rapport a la place dans la matrice
    for i in range(nb, nb + x):
        nomRobot = "@" + str(i)
        placementRobot(apt, nomRobot)
    nombreRobot += x

#### PARTIE SON ####

def detectionSon(Apt):
    for i in range(hauteur):
        for j in range(largeur):
            if "s" in Apt[i][j]:
                return True

def detectionSonAvertissement(Apt):           # Bon y'a 3 fonctions qui ont les memes boucles for mais c'est plus lisible et pratique comme ça
    for i in range(hauteur):
        for j in range(largeur):
            if "s" in Apt[i][j]:
                print("Les robots entendent du bruit... Ils remontent vers la source \n")

def localisationSon(Apt):
    for i in range(hauteur):
        for j in range(largeur):
            if "s" in Apt[i][j]:
                return i,j

def distanceRobotSon(robot,Apt):
    sonI, sonJ = localisationSon(Apt)
    distanceI = abs(robot[1]-sonI)
    distanceJ = abs(robot[2]-sonJ)
    return distanceI+distanceJ

def checkSon(Apt,robot):
    sonI,sonJ = localisationSon(Apt)
    if sonI == robot[1] and sonJ == robot[2]:
        return True

def notifSon(Apt,robot):
    i = robot[1]
    j = robot[2]
    nom = getNomRobot(Apt, i, j)
    Apt[i][j] = nom
    print("Le robot ", nom," est arrivé à la source du bruit ! Il envoie un rapport à l'utilisateur \n ")



#### PARTIE GAZ ####




def detectionGazAvertissement(Apt):
    for i in range(hauteur):
        for j in range(largeur):
            if "g" in Apt[i][j]:
                print("Fuite de gaz dans l'appartement ! Les robots continuent d'explorer... \n")

def detectionGaz(Apt):
    for i in range(hauteur):
        for j in range(largeur):    #ecriture pas terrible mais c pour éviter que ça écrive 2 fois "fuite de gaz"...
            if "g" in Apt[i][j]:
                return True

def localisationGaz(Apt):
    for i in range(hauteur):
        for j in range(largeur):
            if "g" in Apt[i][j]:
                return i,j

def checkGaz(Apt,robot):
    gazI,gazJ = localisationGaz(Apt)
    if gazI == robot[1] and gazJ == robot[2]:
        return True

def fermeGaz(Apt,robot):
    i= robot[1]
    j= robot[2]
    nom = getNomRobot(Apt, i, j)
    Apt[i][j] = nom
    print("Le robot ",nom," a trouvé la source du gaz ! il s'occupe de la fermer. \n")



#### PARTIE UPDATE ####



def updateApt(Apt, robots):

    detectionGazAvertissement(Apt)
    detectionSonAvertissement(Apt)
    global alerteGazEnvoyee

    if detectionGaz(Apt):
        if alerteGazEnvoyee ==False :
            print("Le robot envoie une notification d'alerte gaz à l'utilisateur \n")
            alerteGazEnvoyee = True

    for robot in robots:  # 1 = haut , 2 = droite , 3 = bas , 4 = gauche
        # deplacement interdit si nb > 1  = (2 + last)%4
        action = 0

        if detectionGaz(Apt):
            if checkGaz(Apt,robot):
                fermeGaz(Apt,robot)
                action =1
                alerteGazEnvoyee = False

        if checkFenetre(Apt, robot[1], robot[2]):
            FermeFenetre(Apt, robot[1], robot[2])
            action = 1

        if checkPorte(Apt, robot[1], robot[2]):
            FermePorte(Apt, robot[1], robot[2])
            action = 1

        if detectionSon(Apt):
            if checkSon(Apt,robot):
                notifSon(Apt,robot)
                action = 1
                for i in range(hauteur):
                    for j in range(largeur):
                        if "xt" in Apt[i][j]:
                            Apt[i][j]=re.sub("xt", "", Apt[i][j])

        if detectionSon(Apt):
            if action == 0:
                choix = []
                caseSuivante = []
                if checkHaut(Apt, robot[1], robot[2]):
                    choix.append(1)
                    caseSuivante.append([robot[0],robot[1]-1,robot[2],robot[3]])

                if checkDroite(Apt, robot[1], robot[2]):
                    choix.append(2)
                    caseSuivante.append([robot[0],robot[1],robot[2]+1,robot[3]])

                if checkBas(Apt, robot[1], robot[2]):
                    choix.append(3)
                    caseSuivante.append([robot[0],robot[1]+1,robot[2],robot[3]])

                if checkGauche(Apt, robot[1], robot[2]):
                    choix.append(4)
                    caseSuivante.append([robot[0],robot[1],robot[2]-1,robot[3]])

                if len(choix) == 0 :
                    break

                culSac=False
                if len(choix) == 1 :
                    culSac = True
                departI,departJ = robot[1],robot[2]

                distanceNew = []
                for etats in caseSuivante :
                    distanceNew.append(distanceRobotSon(etats,Apt))

                choixMinis = []
                distanceMini = min(distanceNew)
                for n in range(0,len(distanceNew)):
                    if distanceMini == distanceNew[n]:
                        choixMinis.append(n)

                choixFinaux = []
                for a in choixMinis:
                    choixFinaux.append(choix[a])
                if len(choix) == 2:
                    if (checkHaut(Apt,robot[1],robot[2]) and checkDroite(Apt,robot[1],robot[2])) or (checkDroite(Apt,robot[1],robot[2]) and checkBas(Apt,robot[1],robot[2])) or (checkBas(Apt,robot[1],robot[2]) and checkGauche(Apt,robot[1],robot[2])) or (checkGauche(Apt,robot[1],robot[2]) and checkHaut(Apt,robot[1],robot[2])):
                        #Pour bloquer les coins, on vérifie qu'on ne soit pas bloqué devant/derrière OU droite/gauche. Pour les cas où on a un U de 2x2 par exemple..
                        move = random.choice(choixFinaux)
                        match move:
                            case 1:
                                goHaut(Apt, robot[1], robot[2])
                                robot[3] = 1
                                robot[1] -= 1
                            case 2:
                                goDroite(Apt, robot[1], robot[2])
                                robot[3] = 2
                                robot[2] += 1
                            case 3:
                                goBas(Apt, robot[1], robot[2])
                                robot[3] = 3
                                robot[1] += 1
                            case 4:
                                goGauche(Apt, robot[1], robot[2])
                                robot[3] = 4
                                robot[2] -= 1


                        Apt[departI][departJ] += "xt"  # x temporaire
                    else :
                            if robot[3]!=3 and 1 in choix:
                                goHaut(Apt, robot[1], robot[2])
                                robot[3] = 1
                                robot[1] -= 1
                            elif robot[3]!=4 and 2 in choix:
                                goDroite(Apt, robot[1], robot[2])
                                robot[3] = 2
                                robot[2] += 1
                            elif robot[3]!=1 and 3 in choix:
                                goBas(Apt, robot[1], robot[2])
                                robot[3] = 3
                                robot[1] += 1
                            elif robot[3]!=2 and 4 in choix:
                                goGauche(Apt, robot[1], robot[2])
                                robot[3] = 4
                                robot[2] -= 1

                elif len(choixFinaux) != 1:
                    move = robot[3]
                    while (move == robot[3]):
                        move = random.choice(choixFinaux)

                    match move:
                        case 1:
                            goHaut(Apt, robot[1], robot[2])
                            robot[3] = 1
                            robot[1] -= 1
                        case 2:
                            goDroite(Apt, robot[1], robot[2])
                            robot[3] = 2
                            robot[2] += 1
                        case 3:
                            goBas(Apt, robot[1], robot[2])
                            robot[3] = 3
                            robot[1] += 1
                        case 4:
                            goGauche(Apt, robot[1], robot[2])
                            robot[3] = 4
                            robot[2] -= 1
                else :
                    move = choixFinaux[0]
                    match move:
                        case 1:
                            goHaut(Apt, robot[1], robot[2])
                            robot[3] = 1
                            robot[1] -= 1
                        case 2:
                            goDroite(Apt, robot[1], robot[2])
                            robot[3] = 2
                            robot[2] += 1
                        case 3:
                            goBas(Apt, robot[1], robot[2])
                            robot[3] = 3
                            robot[1] += 1
                        case 4:
                            goGauche(Apt, robot[1], robot[2])
                            robot[3] = 4
                            robot[2] -= 1
                if culSac :
                    Apt[departI][departJ] += "xt"  #x temporaire


        else :

            if action == 0:
                choix = []
                # on vérifie si possible et pas retour en arrière

                if checkHaut(Apt, robot[1], robot[2]):
                    if robot[3] != 3:
                        choix.append(1)
                if checkDroite(Apt, robot[1], robot[2]):
                    if robot[3] != 4:
                        choix.append(2)
                if checkBas(Apt, robot[1], robot[2]):
                    if robot[3] != 1:
                        choix.append(3)
                if checkGauche(Apt, robot[1], robot[2]):
                    if robot[3] != 2:
                        choix.append(4)

                # pas de nouveaux chemins
                if len(choix) == 0:  # cas cul de sac
                    if checkHaut(Apt, robot[1], robot[2]):
                        goHaut(Apt, robot[1], robot[2])
                        robot[3] = 1
                        robot[1] -= 1
                    elif checkDroite(Apt, robot[1], robot[2]):
                        goDroite(Apt, robot[1], robot[2])
                        robot[3] = 2
                        robot[2] += 1
                    elif checkBas(Apt, robot[1], robot[2]):
                        goBas(Apt, robot[1], robot[2])
                        robot[3] = 3
                        robot[1] += 1
                    elif checkGauche(Apt, robot[1], robot[2]):
                        goGauche(Apt, robot[1], robot[2])
                        robot[3] = 4
                        robot[2] -= 1
                    else:  # cas bloqué
                        print(robot[0], " ne bouge pas\n")
                        robot[3] = 0

                if len(choix) > 0:
                    move = random.choice(choix)  # déplacement random rans retour en arrière

                    match move:
                        case 1:
                            goHaut(Apt, robot[1], robot[2])
                            robot[3] = 1
                            robot[1] -= 1
                        case 2:
                            goDroite(Apt, robot[1], robot[2])
                            robot[3] = 2
                            robot[2] += 1
                        case 3:
                            goBas(Apt, robot[1], robot[2])
                            robot[3] = 3
                            robot[1] += 1
                        case 4:
                            goGauche(Apt, robot[1], robot[2])
                            robot[3] = 4
                            robot[2] -= 1


# main
alerteGazEnvoyee = False
hauteur = 5
largeur = 5
apt = [["s ", "fo", "", "@1", " "], [" ", "x", "x", "x", "x"], [" ", "x", "po", " ", "x"], [" ", "x", "f", "@0", "x"],["g", " ", "", "", "p"]]  # createApt()
nombreRobot = 0  # var pour le num robot

# afficherApt(apt)
# setNombreRobot(nombreRobot)
# print("Vos robots sont positionnés : \n")

Robots = positionRobotsinit(apt)  # initialisation des robots ave dernier mouvement null
afficherApt(apt)


while (True):
    updateApt(apt, Robots)
    afficherApt(apt)
    a = input()
# !!! le robot fais la

