PARTIE 2

1) a) Transactionnel car il y aurait beaucoup de lignes inutiles
si on faisait un snapshot ( un client n'achète pas dans plusieurs magasin
chaque jour en général, et il n'achète pas tous les jours.. )

b) additive car on peut sommer le prix total sur tous les attributs


2) a) Transactionnel : chaque ligne est indépendante ( en considérant que le chiffre d'affaire sur chaque produit 
acheté est son prix, et que donc ce n'est pas 
cumulé )

b) additif car on peut sommer le chiffre d'affaire sur tous les attributs 


3) a) Snapshot

b) semi-additif ( on peut sommer sur les différents magasins et sur les produits, mais pas sur les jours )


4) a) Snapshot 

b) semi-additif ( on peut sommer sur les différents magasins, ou sur les produits, mais pas les jours )


5) a) Transactionnel ( aucun intérêt de faire
un snapshot ici, on aurait énormément de lignes
 inutiles ( un même client n'appelle que rarement
 le service ))

b) non-additif : aucune mesure n'existe ici..


6) a) Transctionnel ( les clients ne mettent pas plein de notes chaque jour, donc on aurait encore plein de lignes inutiles si on faisait 
un snapshot )

b) non-additif : on n'additionne pas les notes, cela n'a pas de sens ( ni sur les jours, ni sur les clients ). Mais on ferait plutôt une moyenne


7) a) Transactionnel ( encore une fois, si on faisait un snapshot, on aurait énormément de lignes inutiles car chaque client n'appelle pas chaque jour)

b) additif ( on peut sommer sur le client, sur le jour et sur l'employé )


8) a) Transactionnel. Pour faire les calculs plus tard, mieux vaux ne pas perdre d'information 

b) additif ( on peut sommer sur la monnaie m, sur la banque et sur le jour)


9) a) Snapshot : Sachant que la mesure est le cours de change moyenne pour chaque jour, autant faire un snapshot ( avec une fréquence journalière ) pour ne pas se surcharger d'information qui ne sont pas utile pour ce que l'on mesure

b) non-additif : sommer un cours de change moyen, que ce soit sur la monnaie, la banque ou le jour n'a pas de sens, on ferait plutôt une moyenne.
