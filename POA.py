import random
import re



def createApt() : 
    global hauteur,largeur
    hauteur = int(input("Saisir la hauteur de l'appartement désiré \t"))
    largeur = int(input("Saisir la largeur de l'appartement désiré \t"))
    Apt =[]
    
    print ("on va créer l'appartement \n")
    print(" - Les obstacles sont représentés par un x  \n")
    print(" - Les portes d'entrées sont représentés par po si ouverte p sinon \n")
    print(" - Les fenêtres sont représentés par fo si ouverte f sinon \n")
    print(" - Les espace vides sont représentés par une valeur vide (entrée) \n")
    print(" - Le robot sera représenté par un @")
    inputPermis=["x","","p","f","po","fo"]
    for i in range(hauteur):
        print ("saisir la ligne ",i,"contenant ",largeur ,"valeurs\n")
        r=[]
        for j in range(largeur):
            a=input()
            while(a not in inputPermis) :
                print(" Cet input n'est pas valable, veuillez saisir à nouveau la valeur ",j,"\n")
                a=input()
            r.append(a)
        Apt.append(r)
    return Apt


def afficherApt(apt) :
    for i in range (len (apt)):
        print (apt[i], "\n")
    print('\n')

def placementRobot(apt, nomRobot):
    i=0
    while (i!=1):
        print("Veuillez choisir l'endroit ou vous souhaitez placer le robot \n")
        print("position x du robot\n")
        x = int(input())
        if x in range(0, len(apt)):
            print ("position y du robot\n")
            y = int(input())
            if y in range(0, len(apt[1])):
                if apt[x][y]!="x" and "@" not in apt[x][y]:
                    i=1
                    
                    apt[x][y]= nomRobot + apt[x][y]
        if  (i == 0):
            print("position invalide\n")
        

def positionRobotsinit(Apt):
    cordRobots=[]
    #travers apt
    for i in range (hauteur):
        for j in range(largeur):
            #check chaque string, le robot commence avec @
            if(Apt[i][j].startswith("@",0,1)):
                nom=getNomRobot(Apt,i,j)
                #ajout les cord robots dans le liste
                #premier robot est a cord i,j
                cordRobots.append([nom,i,j,0])# dernier deplacement init a -1,avant 1 il a bougé vers le haut, 2
    return cordRobots


def checkDroite(Apt,i,j):  #i,j emplacement du robot
    return j+1<=largeur-1 and Apt[i][j+1]!="x" and "@" not in Apt[i][j+1]
        
def checkGauche(Apt,i,j):
    return j-1>=0 and Apt[i][j-1]!='x' and "@" not in Apt[i][j-1]

def checkHaut(Apt,i,j):
    return i-1>=0 and Apt[i-1][j]!="x" and  "@" not in Apt[i-1][j]

def checkBas(Apt,i,j):
    return i+1<=hauteur-1 and Apt[i+1][j]!='x' and "@" not in Apt[i+1][j]

def checkAndFermeFenetre(Apt,i,j):
    if('fo' in Apt[i][j]):
        Apt[i][j]=getNomRobot(apt,i,j)+'f'

def checkAndFermePorte(Apt,i,j):
    if('po' in Apt[i][j]) :
        Apt[i][j]= getNomRobot(apt,i,j)+'p'
        
        
        

def goDroite(Apt,i,j):
    nom = getNomRobot(Apt,i,j)
    SupprRobotCase(apt,i,j)
    Apt[i][j+1]=nom+Apt[i][j+1]


def goGauche(Apt,i,j):
    nom = getNomRobot(Apt,i,j)
    SupprRobotCase(Apt,i,j)
    Apt[i][j-1]=nom+Apt[i][j-1]

def goHaut(Apt,i,j):
    nom = getNomRobot(Apt,i,j)
    SupprRobotCase(Apt,i,j)
    Apt[i-1][j]=nom+Apt[i-1][j]

def goBas(Apt,i,j):
    nom = getNomRobot(Apt,i,j)
    SupprRobotCase(Apt,i,j)
    Apt[i+1][j] = nom + Apt[i+1][j]





def getNomRobot(Apt,i,j):
    nomRobot = re.findall("^@[0-9]*",apt[i][j])


    return nomRobot[0]

def SupprRobotCase(Apt,i,j):
    Apt[i][j] = re.sub("^@[0-9]*","",Apt[i][j])

    

def setNombreRobot(nombreRobot):
    print("combien de robot souhaitez vous ajouter ?")
    x = int(input())
    nb = nombreRobot
    # !! on ne fais pas de vérif sur le nombre de robot par rapport a la place dans la matrice
    for i in range(nb,nb + x):
        nomRobot = "@"+str(i)
        placementRobot(apt,nomRobot)
    nombreRobot += x
        

def updateApt(Apt, robots):
    
    for robot in robots : #1 = haut , 2 = droite , 3 = bas , 4 = gauche
                            # deplacement interdit si nb > 1  = (2 + last)%4
                
        
        choix=[]
        # on vérifie si possible et pas retour en arrière
        if checkHaut(Apt,robot[1],robot[2]) and robot[3]!=3:
            choix.append(1)
        if checkDroite(Apt,robot[1],robot[2])and robot[3]!=4:
            choix.append(2)
        if checkBas(Apt,robot[1],robot[2])and robot[3]!=1:
            choix.append(3)
        if checkGauche(Apt,robot[1],robot[2]) and robot[3]!=2 :
            choix.append(4)


        if len(choix) == 0: #cas cul de sac
            if checkHaut(Apt,robot[1],robot[2]) :
                goHaut(Apt,robot[1],robot[2])
                robot[3]=1
            if checkDroite(Apt,robot[1],robot[2]) :
                goDroite(Apt,robot[1],robot[2])
                robot[3]=2
            if checkBas(Apt,robot[1],robot[2]) :
                goBas(Apt,robot[1],robot[2])
                robot[3]=3
            if checkGauche(Apt,robot[1],robot[2]):
                goGauche(Apt,robot[1],robot[2])
                robot[3]=4
            
        if len(choix)>0:
            move = random.choice(choix)# déplacement random rans retour en arrière
        
            match move:
                case 1:
                    goHaut(Apt,robot[1],robot[2])
                    robot[3]=1
                    robot[1]-=1
                case 2:
                    goDroite(Apt,robot[1],robot[2])
                    robot[3]=2
                    robot[2]+=1
                case 3 :
                    goBas(Apt,robot[1],robot[2])
                    robot[3]=3
                    robot[1]+=1
                case 4:
                    goGauche(Apt,robot[1],robot[2])
                    robot[3]=4
                    robot[2]-=1
            
                    



        
#main 
hauteur = 3
largeur =3
apt = [["@0","",""],["","@1",""],["","",""]]#createApt()
nombreRobot = 2 #todo remettre a zero

#afficherApt(apt)
#setNombreRobot(nombreRobot)
#print("Vos robots sont positionnés : \n")
Robots=positionRobotsinit(apt)#initialisation des robots ave dernier mouvement null
afficherApt(apt)

while(True) :
    updateApt(apt,Robots)
    afficherApt(apt)
    a=input()
#!!! le robot fais la verif quand il est SUR la case et non pas une fois qu'il est passé a la case suivante 