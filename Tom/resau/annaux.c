include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>

/* Programme client */

int main(int argc, char *argv[]) {

  /* je passe en paramètre l'adresse de la socket du serveur (IP et
     numéro de port) et un numéro de port à donner à la socket créée plus loin.*/

  /* Je teste le passage de parametres. Le nombre et la nature des
     paramètres sont à adapter en fonction des besoins. Sans ces
     paramètres, l'exécution doit être arrétée, autrement, elle
     aboutira à des erreurs.*/
  if (argc != 4){
    printf("utilisation : %s ip_serveur port_serveur port_client\n", argv[0]);
    exit(1);
  }

  /* Etape 1 : créer une socket */   
  int ds = socket(PF_INET, SOCK_STREAM, 0);
  /*socket(domaine,type,protocole)*/


  /* /!\ : Il est indispensable de tester les valeurs de retour de
     toutes les fonctions et agir en fonction des valeurs
     possibles. Voici un exemple */
  if (ds == -1){
    perror("Client : pb creation socket :");
    exit(1); // je choisis ici d'arrêter le programme car le reste
	     // dépendent de la réussite de la création de la socket.
  }
  
  /* J'ajoute des traces pour comprendre l'exécution et savoir
     localiser des éventuelles erreurs */
  printf("Client : creation de la socket réussie \n");
  
  // Je peux tester l'exécution de cette étape avant de passer à la
  // suite. Faire de même pour la suite : n'attendez pas de tout faire
  // avant de tester.
  

  /* Etape 2 : demande de connection */
  struct sockaddr_in adServ;
  adServ.sin_family = AF_INET;
  adServ.sin_addr.s_addr = inet_addr(argv[1]); // changé un truc la ? 
  adServ.sin_port = htons((short)(atoi(argv[2]))); // changé un truc la ? 
  socklen_t IgA = sizeof(struct sockaddr_in);
  int res = connect(ds,(struct sockaddr *)&adServ,IgA);

  
  if (res==-1){
    printf("une erreur connect");
    close(ds);
    exit(1);
  }
  
 
  /* Etape 4 : envoyer un message au serveur  (voir sujet pour plus de détails)*/

  
  int msg; // passe d'un tableau de char a un int 
  printf("votre message : \n");
  scanf("%s",msg);  
  
  int tt = strlen(msg);
  // on envoie un message si on a pas réussit a tout envoyé on en renvoi un avec le rest du premier ( send (sd,msg+res,size_msg-res))

  ssize_t taille= send(ds,&tt,sizeof(int)+1,0);

  ssize_t conf = send(ds,msg,strlen(msg),0);

  //ssize_t conf2 = send(ds,msg,strlen(msg),0);

  if (conf==-1){
    printf("message non envoye\n");
    close(ds);
    exit(1);
  }
  else {
    printf("msg 1 envoye\n");
  }

  /*
  if (conf2==-1){
    printf("message non envoye\n");
    close(ds);
    exit(1);
  }
  else {
    printf("msg2 envoye\n");
  }
  */

  printf("taille msg envoye : %li\n",strlen(msg));
  
  /* Etape 5 : recevoir un message du serveur (voir sujet pour plus de détails)*/
  int reponse;
  ssize_t t= recv(ds,reponse,sizeof(reponse),0);
  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  if (t == -1){
      printf("pb recvfrom");
      close(ds);
      exit(1);
  }

  printf("reponse du serveur : %s\n", reponse);
  

//c'est surement de la merde


int msg2 = reponse+1; // passe d'un tableau de char a un int 
  printf("votre message : \n");
  scanf("%s",msg2);  
  
  int tt = strlen(msg2);
  // on envoie un message si on a pas réussit a tout envoyé on en renvoi un avec le rest du premier ( send (sd,msg+res,size_msg-res))

  ssize_t taille= send(ds,&tt,sizeof(int)+1,0);

  ssize_t conf = send(ds,msg2,strlen(msg2),0);

  //ssize_t conf2 = send(ds,msg,strlen(msg),0);

  if (conf==-1){
    printf("message non envoye\n");
    close(ds);
    exit(1);
  }
  else {
    printf("msg 1 envoye\n");
  }






  
  close(ds);
  printf("Client : je termine\n");
  return 0;
}
