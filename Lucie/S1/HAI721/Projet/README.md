# Projet de Programmation Repartie

## Utilisation

Un makefile est là pour la compilation. Il suffit de taper la commande make dans un terminal.

```
make
```

Pour exécuter le serveur et tout les processus en même temps il faut utiliser un launcher. 
Attention, il faut avoir mis les descriptions des graphes dans le dossier graphe.

```
./launcher graphe/nomDuGraphe numeroPort
```

Pour exécuter le serveur et les clients sans utilisé le launcher. 
Pour lancer le client il est possible de ne pas saisir l'ip et le port client (IP_client, port_client).

```
./serveur IP_serveur port_serveur nomFichier
./client IP_serveur port_serveur IP_client port_client 

```
