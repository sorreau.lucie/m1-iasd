//coloration rapide mais produit bcp de couleurs
unsigned long long fastColor(int nbVoisins, int listVoisins[], FILE *logClient,int moi)
{
    srand(getpid());
    struct message Message;
    
    int couleur[nbVoisins];
    for(int i =0;i<nbVoisins;i++){
        couleur[i]=0;
    }
    int btemp = 0; // Bit de couleur
    int b = 1;
    int nbdead = 0;
    while (nbdead <nbVoisins)
    {   struct message Message;
        
        btemp = rand() % 2;
        b = b<<1;
        b+=btemp;
        fprintf(logClient, "coltemp  %i  \n",b);
        for (int i = 0; i < nbVoisins; i++) // envoie de ma couleur à mes voisins
        {
            if (couleur[i] != -1) // si le voisin  i est actif
            {
                Message.requete = COULEUR;
                Message.couleur = b;
                Message.information1 = moi;
                
                int envoiecol = send(listVoisins[i], &Message, sizeof(Message), 0);
                if (envoiecol == -1)
                {
                    fprintf(logClient, "FASTCOLOR :Problème d'envoie de couleur : %s \n", strerror(errno));
                }
            }
        }
        for (int i = 0; i < nbVoisins; i++) // reception des couleurs  de mes voisins actifs
        {
            if (couleur[i] != -1) // si le voisin  i est actif
            {
                struct message Message1;
                int recol = recv(listVoisins[i], &Message1, sizeof(Message), 0); // reception de la couleur d'un voisin
                fprintf(logClient, "reception  %i  : %lli \n",Message1.information1,Message1.couleur);
                if (recol < 0)
                {
                    fprintf(logClient, "FASTCOLOR :probleme de reception de couleur : %s \n", strerror(errno));
                }
                if (Message1.couleur != b)
                {couleur[i]=-1;
                nbdead++;}
                
            }
        } 
        
    }
    
    return b;
}

//reduction du nombre de couleurs 
int reduce (int nbVoisins, int listVoisins[], FILE *logClient,int moi,unsigned long long int val){
int envoie[nbVoisins];
int couleur[nbVoisins+1];
couleur[0]=0;
for(int i =0;i<nbVoisins;i++){
    envoie[i]=1; //1 -> val > moi , je choisis ma couleur et je l'envoie a envoie[i],0 il me bloque une couleur
    couleur[i+1]=0;
}
//graphe orienté 
struct message Message;
Message.information1 = moi;
Message.couleur=val;
for (int i=0;i<nbVoisins;i++){//j'envoie ma val à tous mes voisins
    int envoiecol = send(listVoisins[i], &Message, sizeof(Message), 0);
    if (envoiecol == -1)
    {
        fprintf(logClient, "REDUCE :Problème d'envoie de couleur : %s \n", strerror(errno));
    } 
}
int compte=0;
struct message Message1;
//sens du graphe 
for (int i=0;i<nbVoisins;i++){//reception de la couleur de mes voisins
    int recol = recv(listVoisins[i], &Message1, sizeof(Message1), 0); // reception de la couleur d'un voisin
    fprintf(logClient, " REDUCE :reception  %i  : %lli \n",Message1.information1,Message1.couleur);
    if (recol < 0)
    {
        fprintf(logClient, "REDUCE :probleme de reception de couleur : %s \n", strerror(errno));
    } 
    if (Message1.couleur<val){//il est plus prioritaire que moi
        envoie[i]=0;
    }
}
//reception des couleurs utilisées
for(int i=0;i<nbVoisins;i++){
if(envoie[i]==0){
    int recol = recv(listVoisins[i], &Message1, sizeof(Message1), 0); // reception de la couleur d'un voisin
        if (recol < 0)
    {
        fprintf(logClient, "REDUCE :probleme de reception de couleur bloquée: %s \n", strerror(errno));
    } 
    couleur[Message1.information]=1;//on bloque la couleur
    fprintf(logClient, " REDUCE : %i bloque : %i \n",Message1.information1,Message1.information);
    compte++;}
}
//choix d'une couleur
for(int i=0;i<nbVoisins+1;i++){
    if(couleur[i]==0){
        Message.information=i;
        for(int j=0;j<nbVoisins;j++){
            if(envoie[j]==1){
            int envoiecol = send(listVoisins[j], &Message, sizeof(Message), 0);
            if (envoiecol == -1) {
                fprintf(logClient, "REDUCE :Problème d'envoie de couleur bloquée : %s \n", strerror(errno));
            } 
        compte++;
            }
        }
    fprintf(logClient, "test : %i  nbvoisins : %i \n",compte,nbVoisins);
    fprintf(logClient, "Ma couleur après reduce est : %i \n",i);

    return i;
    }
}
}
