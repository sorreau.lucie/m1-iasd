#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include "outils.c"
#include "fast_color.c"

int main(int argc, char *argv[])
{
    if (argc != 5)
    {
        if (argc != 3)
        {
            // todo Changer nb arg pour port client et ip client
            printf("Utilisation : %s  ip_serveur port_serveur ip_client port_client \n ip_client initialisé si NULL\n port_client initialisé si NULL \n", argv[0]);
            exit(1);
        }
    }
    time_t begin = time(NULL);
    int moi = 0;
    int nbClientConnecte = 0;

    int ds = socket(PF_INET, SOCK_STREAM, 0);
    if (ds == -1)
    {
        perror("Client : Problème d'initialisation de socket");
        exit(1);
    }

    //  adresse de la socket client
    struct sockaddr_in ad;
    ad.sin_family = AF_INET;
    // ARGC 5
    if (argc == 5)
    {
        inet_pton(AF_INET, argv[3], &(ad.sin_addr));
        ad.sin_port = htons((short)atoi(argv[4]));
    }
    else
    { // argc 3
        ad.sin_addr.s_addr = INADDR_ANY;
        ad.sin_port = 0;
    }
    int res;
    res = bind(ds, (struct sockaddr *)&ad, sizeof(ad));
    if (res == -1)
    {
        printf("Client: Pb socket\n");
    }

    // connexion
    // adresse a laquel on bind (serveur)
    struct sockaddr_in adserveur;
    adserveur.sin_family = AF_INET;
    inet_pton(AF_INET, argv[1], &(adserveur.sin_addr));
    adserveur.sin_port = htons((short)atoi(argv[2]));
    socklen_t lg_Adr = sizeof(struct sockaddr_in);

    int resultConnexion = connect(ds, (struct sockaddr *)&adserveur, lg_Adr);

    if (resultConnexion == -1)
    {
        perror("Client : Problème de connexion");
        exit(1);
    }

    struct message Message;

    // socket ecoute

    int dsListen = socket(PF_INET, SOCK_STREAM, 0);
    int option = 1;
    setsockopt(dsListen, SOL_SOCKET, SO_REUSEADDR, (char *)&option, sizeof(option));
    int resBind = bind(dsListen, (struct sockaddr *)&ad, lg_Adr);
    if (resBind == -1)
    {
        perror("Problème de bind dslisten");
        exit(1);
    }

    int dsvoisin = socket(PF_INET, SOCK_STREAM, 0);

    struct sockaddr_in infos;
    socklen_t lg_info = sizeof(infos);
    getsockname(dsListen, (struct sockaddr *)&infos, &lg_info); // recup port
    int infoport = infos.sin_port;
    getsockname(ds, (struct sockaddr *)&infos, &lg_info); // recup ip
    infos.sin_port = infoport;
    Message.adresse = infos;
    send(ds, &Message, sizeof(Message), 0);
    int reception = recv(ds, &Message, sizeof(Message), 0); // premier mess serv
    if (reception < 0)
    {
        perror("probleme de reception du message du serveur \n ");
    }
    int nbVoisins = Message.information;
    moi = Message.qui; // numero de sommet
    int socketlist[nbVoisins];
    FILE *logClient = NULL;
    char nomFichier[500] = "";
    strcat(nomFichier, "log/");

    // Creation fichier log
    char idMoi[100];
    sprintf(idMoi, "%d", moi);
    strcat(nomFichier, idMoi);
    strcat(nomFichier, ".txt");

    logClient = fopen(nomFichier, "w");
    if (logClient != NULL)
    {
        fputs("Fichier crée \n", logClient);
    }

    listen(dsListen, nbVoisins);
    time_t apresnbvoisin = time(NULL);

    unsigned long secondes = (unsigned long)difftime(apresnbvoisin, begin);
    fprintf(logClient, "temps apres nb de voisin : %i \n", (char)secondes);
    fflush(logClient);
    int envoie = send(ds, &Message, sizeof(Message), 0);
    time_t apresconfirm = time(NULL);
    secondes = (unsigned long)difftime(apresconfirm, apresnbvoisin);
    fprintf(logClient, "temps apres confirmation nb voisin : %i \n", (char)secondes);
    fflush(logClient);

    fd_set set, settmp;
    FD_ZERO(&set);
    FD_SET(dsListen, &set);
    FD_SET(ds, &set);
    int max = (dsListen > ds) ? dsListen : ds;

    while (1) // creation du graphe
    {
        settmp = set;
        int resSelect = select(max + 1, &settmp, NULL, NULL, NULL);

        if (resSelect == -1)
        {
            fprintf(logClient, " probleme de select %s \n", strerror(errno));
            fflush(logClient);
            // perror("Client : Probleme de select");
            exit(1);
        }
        for (int df = 2; df <= max; df++)
        {
            if (!FD_ISSET(df, &settmp))
            {
                continue;
            }
            if (df == dsListen)
            {
                int dsc = accept(dsListen, NULL, NULL);
                if (dsc > 0)
                {
                    fprintf(logClient, " accept : %i \n", dsc);
                    fflush(logClient);
                }
                if (dsc == -1)
                {
                    fprintf(logClient, " probleme d'accepte dsListen %s \n", strerror(errno));
                    fflush(logClient);
                    exit(1);
                }
                fprintf(logClient, "reception d'un voisin \n");
                fflush(logClient);

                socketlist[nbClientConnecte] = dsc;
                nbClientConnecte++;
                continue;
            }
            if (df == ds)
            {
                int reception = recv(ds, &Message, sizeof(Message), 0);
                if (reception < 0)
                {
                    fprintf(logClient, " probleme de reception du message du serveur %s \n", strerror(errno));
                    fflush(logClient);
                    // perror("Client : probleme de reception du message du serveur \n ");
                    exit(1);
                }
                if (reception == 0)
                {
                    FD_CLR(ds, &set);
                    continue;
                }
                if (Message.requete == CONNECT)
                {
                    fprintf(logClient, "Connexion de %i a %i \n", moi, Message.qui);
                    fflush(logClient);

                    int dsvoisin = socket(PF_INET, SOCK_STREAM, 0);
                    if (dsvoisin == -1)
                    {
                        fprintf(logClient, "Problème de création socket voisin %s \n", strerror(errno));
                        fflush(logClient);
                    }
                    int resultConnexion = connect(dsvoisin, (struct sockaddr *)&Message.adresse, lg_Adr);
                    if (resultConnexion == -1)
                    {
                        fprintf(logClient, " Problème de connexion voisin %s \n", strerror(errno));
                        fflush(logClient);
                        // perror("Client : Problème de connexion voisin");
                        exit(1);
                    }
                    fprintf(logClient, "Connect : %i \n", resultConnexion);
                    fflush(logClient);
                    socketlist[nbClientConnecte] = dsvoisin;
                    nbClientConnecte++;

                    continue;
                }
            }
        }
        fprintf(logClient, "nb client connectés : %i / %i \n", nbClientConnecte, nbVoisins);
        fflush(logClient);
        if (nbVoisins == nbClientConnecte)
        {
            send(ds, &Message, sizeof(Message), 0);
            break;
        }
    }
    // écriture dans les logs
        time_t apresvoisin = time(NULL);
        secondes = (unsigned long)difftime(apresvoisin, apresconfirm);
        fprintf(logClient, "temps apres connexions avec les voisins : %i \n", (char)secondes);
        fflush(logClient);

        fprintf(logClient, "j ai %d connexions \n", nbClientConnecte);
        fflush(logClient);

        printf("client : %i : j ai %d connexions \n", moi, nbClientConnecte);

    // coloration
        unsigned long long int couleurtemp = fastColor(nbVoisins, socketlist, logClient, moi);
        fprintf(logClient, "ma couleur : %llu \n", couleurtemp);
        int couleur = reduce(nbVoisins, socketlist, logClient, moi,couleurtemp);
        // send couleur serv
        Message.information = couleur;
       // Message.couleur = couleurtemp;
        send(ds, &Message, sizeof(Message), 0);

    // fermeture des sockets
        int fermeture = close(ds);
        fprintf(logClient, "Descripteur de socket Client : %i \n", ds);
        fflush(logClient);

        if (fermeture == -1)
        {
            perror("Client :Problème de fermeture socket \n");
            exit(1);
        }
        for (int i = 0; i < nbVoisins; i++) // fermeture socket voisins
        {
            fprintf(logClient, "Descripteur de socket voisins : %i \n", socketlist[i]);
            fflush(logClient);

            int fermeture = close(socketlist[i]);
            if (fermeture == -1)
            {
                perror("Client : Problème de fermeture socket voisins \n");
                exit(1);
            }
    }
    fclose(logClient);

    return 0;
}
