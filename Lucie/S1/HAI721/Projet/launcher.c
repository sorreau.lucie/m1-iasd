#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/time.h> 
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>

int main(int argc, char *argv[])
{
    int max_size = 1000;
    int nbsommet = 0;
    char *ligne = NULL;
    ssize_t read;
    ssize_t len = 0;
    FILE *fp;
    if (argc != 3)
    {
        printf("utilisation : %s  nomFichier NuméroPort\n", argv[0]);
        exit(1);
    }

    DIR *directory;
    struct dirent *entry;
    struct stat file_stat;
    char const* nameDir;
    nameDir = "log/";
    char buffer[1024] = {0};

    directory = opendir(nameDir);
    if ( directory == NULL ) {
        fprintf(stderr, "cannot open directory %s\n", nameDir);
        return 0;
    }

    while ( (entry = readdir(directory)) != NULL ) {

        if ( strcmp(entry->d_name, ".") == 0 ||
             strcmp(entry->d_name, "..") == 0 ) {
            continue;
        }

        snprintf(buffer, 1024, "%s/%s", nameDir, entry->d_name);
        stat(buffer, &file_stat);
        if ( S_ISREG(file_stat.st_mode) ) {
            remove(buffer);
        }
    }

    FILE *fichier = fopen(argv[1], "r");
    if (fichier == NULL)
    {
        perror("echec a la lecture du graphe");
    }

    // On va ignorer les ligne avant les informations
    while (((read = getline(&ligne, &len, fichier)) != -1) && (ligne[0] != 'p')){}
    char buflecture[50];
    sscanf(ligne, "p %[^ ] %i", (char *)&buflecture, &nbsommet);
    char cmd[100]="";
    strcat(cmd,"./serveur 127.0.0.1 ");
    strcat(cmd,argv[2]);
    strcat(cmd," ");
    strcat(cmd,argv[1]);
    printf("cmd : %s\n", cmd);

    fp = popen(cmd,"r");
    sleep(1);
    printf("Lancement du Serveur \n");
    if(fp == NULL){
        perror("Problème de lancement serveur \n");
        exit(1);
    }
    char cmdC[100]="";
    strcat(cmdC,"./client 127.0.0.1 ");
    strcat(cmdC,argv[2]);
    printf("cmdC : %s \n", cmdC);
    for (int i = 0; i < nbsommet; i++)
    {
        fp = popen(cmdC,"r");
        printf("Lancement du Processus : %i \n", i+1);
        if(fp == NULL){
            perror("Problème de lancement du client\n");
            exit(1);
        }
    }
    sleep(1);
if (nbsommet<30)
{
    char cmdA[100] = "";
    strcat(cmdA,"python3 affichage.py ");
    strcat(cmdA,argv[1]);
    fp = popen(cmdA,"r");
    printf("Lancement de l'affichage \n");
    if(fp == NULL){
        perror("Problème de lancement de l'affichage \n");
       exit(1);
    }}

    fclose(fichier);
    pclose(fp);

    return 0;
}