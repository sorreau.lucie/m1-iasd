import igraph as ig
import matplotlib.pyplot as plt
import sys

# Construct a graph with 3 vertices

fo = open (sys.argv[1],"r+")
fo1= open("log/couleur.txt","r+")
edges = []
color=[]

ligne= fo.readlines()
for i in ligne: 
    liens = i.split() 
    if (liens[0]=='e'):
        a = [int(liens[1])-1,int(liens[2])-1]
        edges.append(a)


g = ig.Graph( edges)

couleurs = fo1.readlines()
for i in couleurs:
    color.append(i)


g.vs["col"]=color
# Plot in matplotlib
# Note that attributes can be set globally (e.g. vertex_size), or set individually using arrays (e.g. vertex_color)
fig, ax = plt.subplots(figsize=(100,100))

ig.autocurve(g,None)

ig.plot(
    g,
    target=ax,
    layout="auto", 
    vertex_size=0.1,
    #vertex_color=["steelblue"],
    vertex_label=g.vs["col"],
    vertex_frame_width=7.0,
    vertex_frame_color="white",
    vertex_label_size=7.0,
    edge_color=["#7142cf"]
)
fo.close()
fo1.close()
plt.show()


