#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "outils.c"

int main(int argc, char *argv[])
{

    int max_size = 1000;
    int nbsommet = 0;
    char *ligne = NULL;
    ssize_t read;
    ssize_t len = 0;
    if (argc != 4)
    {
        printf("utilisation : %s IP_serveur port_serveur nomFichier \n", argv[0]);
        exit(1);
    }
    FILE *logtest = fopen("test.txt", "r+");
    FILE *fichier = fopen(argv[3], "r");
    if (fichier == NULL)
    {
        perror("echec a la lecture du graphe");
    }

    // On va ignorer les ligne avant les informations
    while (((read = getline(&ligne, &len, fichier)) != -1) && (ligne[0] != 'p'))
    {
    }
    char buflecture[50];
    sscanf(ligne, "p %[^ ] %i", (char *)&buflecture, &nbsommet);

    int nbvoisins[nbsommet];
    for (int i = 0; i < nbsommet; i++)
        nbvoisins[i] = 0;

    // on recupere les liaisons
    while (read = getline(&ligne, &len, fichier) != -1)
    {
        int i;
        int j;
        sscanf(ligne, "e %i %i", &i, &j);
        nbvoisins[i - 1]++;
        nbvoisins[j - 1]++;
    }
    fclose(fichier);
    printf("DEBUT \n");

    FILE *logServeur = NULL;
    char nomFichier[500] = "";
    strcat(nomFichier, "log/");

    strcat(nomFichier, "serveur.txt");

    logServeur = fopen(nomFichier, "w");
    if (logServeur != NULL)
    {
        fputs("Fichier crée \n", logServeur);
    }

    int ds = socket(PF_INET, SOCK_STREAM, 0);
    if (ds == -1)
    {
        perror("Serveur : pb creation socket :  \n");
        exit(1);
    }

    printf("apres creation socket \n");
    struct sockaddr_in adress;
    adress.sin_family = AF_INET;
    inet_pton(AF_INET, argv[1], &(adress.sin_addr));
    adress.sin_port = htons((short)atoi(argv[2]));

    printf("bind socket \n");
    // bind de la socket
    int resultBind;
    resultBind = bind(ds, (struct sockaddr *)&adress, sizeof(adress));
    if (resultBind == -1)
    {
        perror("Problème de création de socket \n");
        exit(1);
    }
    printf("bind reussis\n");
    // ecoute de la socket
    int resultListen;
    resultListen = listen(ds, nbsommet);
    if (resultListen == -1)
    {
        perror("Problème de mise en écoute\n");
        exit(1);
    }
    printf("listen reussis\n");
    struct sockaddr_in addrclient;
    socklen_t lgAdr = sizeof(addrclient);

    // tableau des adresses et ports des noeuds
    struct sockaddr_in connexions[nbsommet];
    int nbconnecte = 0;

    int iclient = 0; // var pour remplir le tableau socket client
    int socketclients[nbsommet];
    while (nbconnecte < nbsommet) // recuperation des adresses des  sommets
    {
        // accept la connexion
        int dsClient = accept(ds, (struct sockaddr *)&addrclient, &lgAdr);
        socketclients[iclient] = dsClient;
        char str[INET_ADDRSTRLEN];
        if (dsClient == -1)
        {
            perror("Problème d'acceptation de la connexion\n");
            exit(1);
        }
        struct message MessageAdresseSocketClient;
        int reception = recv(dsClient, &MessageAdresseSocketClient, sizeof(MessageAdresseSocketClient), 0);
        if (reception == -1)
        {
            perror("Problème de reception du message \n");
            exit(1);
        }

        // ajout au tableau de connexions
        connexions[nbconnecte] = MessageAdresseSocketClient.adresse; // adresse

        nbconnecte++;
        fprintf(logServeur, "nb de clients écouté : %d\n", nbconnecte);
        iclient++;
    }

    for (int i = 0; i < nbsommet; i++) // envoie du nombre de voisins
    {
        struct message Messageintro;
        Messageintro.information = nbvoisins[i];

        Messageintro.qui = i + 1;
        int envoieMessage = send(socketclients[i], &Messageintro, sizeof(Messageintro), 0);
        if (envoieMessage == -1)
        {
            fprintf(logServeur, "Problème d'envoie de message : %s \n", strerror(errno));
        }
    }
    nbconnecte = 0;
    while (nbconnecte < nbsommet) // reception nb voisin ok
    {

        struct message Message;
        int reception = recv(socketclients[nbconnecte], &Message, sizeof(Message), 0);
        if (reception == -1)
        {
            fprintf(logServeur, "Problème de reception de message : %s \n", strerror(errno));
        }
        nbconnecte++;
    }
    // envoie  des adresses des voisins
    {
        FILE *fichier1 = fopen(argv[3], "r");
        if (fichier == NULL)
        {
            perror("echec a la lecture du graphe");
        }

        // On va ignorer les ligne avant les informations
        while (((read = getline(&ligne, &len, fichier1)) != -1) && (ligne[0] != 'p'))
        {
        }
        sscanf(ligne, "p %[^ ] %i", (char *)&buflecture, &nbsommet);
        // on recupere les liaisons
        while (read = getline(&ligne, &len, fichier1) != -1)
        {
            int i;
            int j;
            sscanf(ligne, "e %i %i", &i, &j);

            struct message Message;
            Message.requete = CONNECT;
            Message.adresse = connexions[j - 1];
            Message.qui = j;
            int envoieAdresse = send(socketclients[i - 1], &Message, sizeof(Message), 0);
            if (envoieAdresse == -1)
            {
                fprintf(logServeur, "Problème d'envoie d'adresse : %s \n", strerror(errno));
            }
            fprintf(logServeur, "requête de %i à %i \n", i, j);
        }
        fclose(fichier1);
    }
    // verif tous les clients ok
    {
    
        nbconnecte = 0;
        while (nbconnecte < nbsommet) // reception nb voisin ok
        {

            struct message Message;
            int reception = recv(socketclients[nbconnecte], &Message, sizeof(Message), 0);
            if (reception == -1)
            {
                fprintf(logServeur, "Problème de reception de message : %s \n", strerror(errno));
            }
            nbconnecte++;
            fprintf(logServeur,"nb client ok : %d \n",nbconnecte);
            printf("nb client ok : %d \n",nbconnecte);
        }
    }


    FILE *logCouleur = NULL;
    char nomCouleur[500] = "";
    strcat(nomCouleur, "log/");

    strcat(nomCouleur, "couleur.txt");

    logCouleur = fopen(nomCouleur, "w");

// recv couleur
int test=0;
        nbconnecte = 0;
        int max=0;
        int couleur[nbsommet];
        while (nbconnecte < nbsommet) // reception couleur
        {
            struct message Message;
            int reception = recv(socketclients[nbconnecte], &Message, sizeof(Message), 0);
            if (reception == -1)
            {
                fprintf(logCouleur, "Problème de reception de couleur : %s \n", strerror(errno));
            }

            if(max<Message.information)
            max=Message.information;
            
            fprintf(logCouleur,"%i\n", Message.information);
            couleur[nbconnecte]=Message.information;
            nbconnecte++;
        } 

/*         while (nbconnecte < nbsommet) // reception couleur
        {
            struct message Message;
            int reception = recv(socketclients[nbconnecte], &Message, sizeof(Message), 0);
            if (reception == -1)
            {
                fprintf(logCouleur, "Problème de reception de couleur : %s \n", strerror(errno));
            }
            
            fprintf(logCouleur,"%llu \n", Message.couleur);
            couleur[nbconnecte]=Message.couleur;
            nbconnecte++;
        } */
        fprintf(logCouleur,"nb couleur : %i\n", max);
        fseek(logtest,0,SEEK_END);
        fprintf(logtest,"%i\n", max);

//verification
 FILE *fichierverif = fopen(argv[3], "r");

    if (fichier == NULL)
    {
        perror("echec a la lecture du graphe");
    }

    // On va ignorer les ligne avant les informations
    while (((read = getline(&ligne, &len, fichierverif)) != -1) && (ligne[0] != 'p'))
    {
    }
   
    sscanf(ligne, "p %[^ ] %i", (char *)&buflecture, &nbsommet);

    
    for (int i = 0; i < nbsommet; i++)
        nbvoisins[i] = 0;

    // on recupere les liaisons
    while (read = getline(&ligne, &len, fichierverif) != -1)
    {
        int i;
        int j;
        sscanf(ligne, "e %i %i", &i, &j);
        if(couleur[i-1]==couleur[j-1]){
            fprintf(logCouleur,"erreur coloration   %i  ::  %i\n", i,j);
            test=1;
        }

    }
    fclose(fichierverif);
    fprintf(logCouleur,"test  %i\n", test);
    


    {//fermeture sockets
    int fermeture = close(ds);
    if (fermeture == -1)
    {
        perror("Serveur : Problème de fermeture socket \n");
        exit(1);
    }
    for (int i = 0; i < nbsommet; i++)
    {

        close(socketclients[i]);
        if (fermeture == -1)
        {
            perror("Serveur : Problème de fermeture socket clients \n");
            exit(1);
        }
    }
    fclose(logtest);
    fclose(logServeur);}
    return 0;
}