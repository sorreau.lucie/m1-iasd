package geo;

public class Vecteur2D extends Vecteur {
    public Vecteur2D(double x, double y) {
        super(2);
        coords[0] = x;
        coords[1] = y;
    }
    
    public double sinus() {
        // Calcule le sinus de l'angle entre le vecteur et l'axe des abscisses
        return coords[1] / this.norme();
    }
    
    public double sinus(Vecteur2D v) {
        // Calcule le sinus de l'angle entre ce vecteur et un autre vecteur 'v'
        return det(v) / (this.norme() * v.norme());
    }
    
    private double det(Vecteur2D v) {
        // Calcule le déterminant entre ce vecteur et un autre vecteur 'v'
        return (coords[0] * v.coords[1]) - (coords[1] * v.coords[0]);
    }

    public double cosinus() {
        // Calcule le cosinus de l'angle entre le vecteur et l'axe des abscisses
        return coords[0] / this.norme();
    }
    
    public double tangente() {
        return coords[1] / coords[0];
    }
    
    public double angle() {
        // Calcule l'angle en radians entre le vecteur et l'axe des abscisses
        return Math.atan2(coords[1], coords[0]);
    }

    public void setCoords(double x, double y) {
        this.coords[0] = x;
        this.coords[1] = y;
    }

    public double angle(Vecteur2D s2) {
        // Calcule l'angle en radians entre ce vecteur et un autre vecteur 's2'
        return Math.atan2(det(s2), produitScalaire(s2));
    }
}

