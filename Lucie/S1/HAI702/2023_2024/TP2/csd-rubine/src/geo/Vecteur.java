package geo;

public class Vecteur {
	double coords[];
	
	public Vecteur(int dimension) {
		coords = new double[dimension];
	}
	
	public Vecteur(double[] bk) {
		coords = new double[bk.length];
		for (int i = 0; i < bk.length; i++)
			coords[i] = bk[i];
	}

	public Vecteur(Vecteur v) {
		coords = new double[v.getDimension()];
		for (int i = 0; i < coords.length; i++)
			coords[i] = v.get(i);
	}

	public double produitScalaire(Vecteur v) {
   
	    double produit = 0;
	    for (int i = 0; i < this.getDimension(); i++) {
	        produit += this.get(i) * v.get(i);
	    }
	    return produit;
	}
	
	public double cosinus(Vecteur v) {
	    double produitScalaire = this.produitScalaire(v);
	    double normeProduit = this.norme() * v.norme();
	    return produitScalaire / normeProduit;
	}
	

	public double norme() {
	    double sommeCarres = 0;
	    for (int i = 0; i < this.getDimension(); i++) {
	        sommeCarres += Math.pow(this.get(i), 2);
	    }
	    return Math.sqrt(sommeCarres);
	}

	public double get(int i) {
		return coords[i];
	}

	public void setValueForFeature(int i, double d) {
		this.coords[i] = d;		
	}

	public int getDimension() {
		return this.coords.length;
	}
	
}
