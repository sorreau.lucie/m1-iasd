package geo;

import java.util.ArrayList;

//matrices carrees
public class Matrice {
	private final double[][] m;
	private final int dimension;
	private double determinant;

	public Matrice(double[][] coefs) {
		dimension = coefs[0].length;
		m = new double[dimension][dimension];
		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				m[i][j] = coefs[i][j];
			}
		}
	}
	
	public int getDimension() {
        return dimension;
    }

	public static Matrice computeAverageMatrixFromMatrixList(ArrayList<Matrice> matrices) {
	    if (matrices.isEmpty()) {
	        return null; // Gérer le cas où la liste est vide.
	    }

	    int dimension = matrices.get(0).getDimension();
	    double[][] sumMatrix = new double[dimension][dimension];

	    for (Matrice matrix : matrices) {
	        for (int i = 0; i < dimension; i++) {
	            for (int j = 0; j < dimension; j++) {
	                sumMatrix[i][j] += matrix.get(i, j);
	            }
	        }
	    }

	    // Divisez la somme par le nombre de matrices pour obtenir la moyenne.
	    for (int i = 0; i < dimension; i++) {
	        for (int j = 0; j < dimension; j++) {
	            sumMatrix[i][j] /= matrices.size();
	        }
	    }

	    return new Matrice(sumMatrix);
	}

	private double get(int i, int j) {
        return m[i][j];
    }
	
	public Vecteur getColumn(int k) {
        if (k >= dimension) {
            return null; // Gérer le cas où k est en dehors des limites.
        }

        double[] columnData = new double[dimension];
        for (int i = 0; i < dimension; i++) {
            columnData[i] = m[i][k];
        }

        return new Vecteur(columnData);
    }

	public Matrice inverse() {
	    // Vous devez commencer par vérifier si la matrice est carrée (même nombre de lignes et de colonnes).
	    if (dimension != m[0].length) {
	        return null; // La matrice n'est pas carrée, donc elle n'a pas d'inverse.
	    }

	    // Créez une matrice identité de la même dimension que la matrice d'origine.
	    double[][] identityMatrix = new double[dimension][dimension];
	    for (int i = 0; i < dimension; i++) {
	        for (int j = 0; j < dimension; j++) {
	            identityMatrix[i][j] = (i == j) ? 1.0 : 0.0;
	        }
	    }

	    // Effectuez la méthode du pivot de Gauss-Jordan pour transformer la matrice d'origine en la matrice identité.
	    double[][] augmentedMatrix = new double[dimension][2 * dimension];
	    for (int i = 0; i < dimension; i++) {
	        for (int j = 0; j < dimension; j++) {
	            augmentedMatrix[i][j] = m[i][j];
	            augmentedMatrix[i][j + dimension] = identityMatrix[i][j];
	        }
	    }

	    // Réalisez les opérations pour obtenir une matrice échelonnée réduite.
	    for (int k = 0; k < dimension; k++) {
	        double pivot = augmentedMatrix[k][k];
	        if (pivot == 0.0) {
	            // Gérer le cas où le pivot est nul (la matrice n'est pas inversible).
	            return null;
	        }

	        for (int j = 0; j < 2 * dimension; j++) {
	            augmentedMatrix[k][j] /= pivot;
	        }

	        for (int i = 0; i < dimension; i++) {
	            if (i != k) {
	                double factor = augmentedMatrix[i][k];
	                for (int j = 0; j < 2 * dimension; j++) {
	                    augmentedMatrix[i][j] -= factor * augmentedMatrix[k][j];
	                }
	            }
	        }
	    }

	    // Extraire la matrice inverse à partir de la matrice échelonnée réduite.
	    double[][] inverseMatrix = new double[dimension][dimension];
	    for (int i = 0; i < dimension; i++) {
	        for (int j = 0; j < dimension; j++) {
	            inverseMatrix[i][j] = augmentedMatrix[i][j + dimension];
	        }
	    }

	    return new Matrice(inverseMatrix);
	}
	
	private void computeDeterminant(int i, int j) {
	    // Cette fonction calcule le déterminant en utilisant l'expansion en cofacteurs.
	    // L'idée est de diviser la matrice en sous-matrices plus petites.

	    // Si la matrice est de taille 1x1, le déterminant est simplement la valeur de cette unique cellule.
	    if (dimension == 1) {
	        determinant = m[0][0];
	    } else if (dimension == 2) {
	        // Si la matrice est de taille 2x2, le déterminant est calculé comme suit :
	        // determinant = m[0][0] * m[1][1] - m[0][1] * m[1][0]
	        determinant = m[0][0] * m[1][1] - m[0][1] * m[1][0];
	    } else {
	        // Pour les matrices de taille supérieure à 2x2, nous utilisons l'expansion en cofacteurs.
	        // Choisissez une ligne ou une colonne (disons la ligne i) et calculez le déterminant
	        // comme la somme des produits des éléments de cette ligne par les déterminants des cofacteurs.
	        // determinant = Σ(j=0 à dimension-1) [m[i][j] * cofactor(i, j)]
	        
	        determinant = 0;
	        for (int k = 0; k < dimension; k++) {
	            // Calculez le cofacteur en excluant la ligne i et la colonne j.
	            double cofactor = cofactor(i, k);
	            
	            // Ajoutez ou soustrayez le produit de l'élément m[i][k] par le cofacteur en fonction de k.
	            if (k % 2 == 0) {
	                determinant += m[i][k] * cofactor;
	            } else {
	                determinant -= m[i][k] * cofactor;
	            }
	        }
	    }
	}

	private double cofactor(int row, int col) {
	    // Cette fonction calcule le cofacteur d'une cellule donnée en excluant la ligne row et la colonne col.
	    // Pour calculer le cofacteur, il est nécessaire de calculer le déterminant de la sous-matrice.
	    // Vous pouvez le faire en appelant récursivement la fonction computeDeterminant.
	    
	    // Créez une sous-matrice en excluant la ligne row et la colonne col.
	    double[][] subMatrix = new double[dimension - 1][dimension - 1];
	    int subI = 0, subJ = 0;
	    
	    for (int i = 0; i < dimension; i++) {
	        if (i != row) {
	            for (int j = 0; j < dimension; j++) {
	                if (j != col) {
	                    subMatrix[subI][subJ] = m[i][j];
	                    subJ++;
	                }
	            }
	            subI++;
	            subJ = 0;
	        }
	    }

	    // Calculez le déterminant de la sous-matrice en appelant computeDeterminant récursivement.
	    Matrice subMatrixObject = new Matrice(subMatrix);
	    subMatrixObject.computeDeterminant(0, 0); // L'appel récursif calcule le déterminant de la sous-matrice.

	    return subMatrixObject.getDeterminant();
	}

	public double getDeterminant() {
	    return determinant;
	}
}
