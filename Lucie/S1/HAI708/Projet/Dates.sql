create table Dates (
	idDate INT,
	dates DATE,
	jour INT,
	mois INT,
	annee INT,
	numJourSemaine INT,
	numJourMois INT,
	numJourAnnee INT,
	numeroTrimestre INT,
	vacances VARCHAR(50)
);
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (1, '7/30/2022', 30, 7, 2022, 3, 30, 213, 4, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (2, '9/15/2022', 15, 9, 2022, 7, 15, 277, 1, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (3, '4/28/2022', 28, 4, 2022, 3, 28, 195, 1, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (4, '1/2/2022', 2, 1, 2022, 7, 2, 218, 3, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (5, '12/11/2021', 11, 12, 2021, 4, 11, 118, 4, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (6, '4/26/2022', 26, 4, 2022, 5, 26, 244, 2, 'Hiver');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (7, '4/18/2022', 18, 4, 2022, 6, 18, 166, 1, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (8, '7/5/2022', 5, 7, 2022, 5, 5, 342, 2, 'Noel');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (9, '3/5/2022', 5, 3, 2022, 3, 5, 6, 2, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (10, '1/20/2022', 20, 1, 2022, 7, 20, 245, 2, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (11, '7/19/2022', 19, 7, 2022, 3, 19, 284, 2, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (12, '7/1/2022', 1, 7, 2022, 2, 1, 110, 2, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (13, '12/14/2021', 14, 12, 2021, 3, 14, 49, 4, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (14, '10/15/2022', 15, 10, 2022, 3, 15, 139, 4, 'Toussaint');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (15, '5/29/2022', 29, 5, 2022, 6, 29, 28, 3, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (16, '4/21/2022', 21, 4, 2022, 2, 21, 226, 1, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (17, '9/28/2022', 28, 9, 2022, 3, 28, 13, 1, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (18, '9/7/2022', 7, 9, 2022, 7, 7, 100, 4, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (19, '5/29/2022', 29, 5, 2022, 6, 29, 332, 4, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (20, '4/20/2022', 20, 4, 2022, 4, 20, 359, 4, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (21, '9/22/2022', 22, 9, 2022, 3, 22, 229, 4, 'Noel');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (22, '11/14/2022', 14, 11, 2022, 2, 14, 106, 2, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (23, '12/21/2021', 21, 12, 2021, 7, 21, 291, 3, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (24, '6/17/2022', 17, 6, 2022, 1, 17, 83, 3, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (25, '8/31/2022', 31, 8, 2022, 7, 31, 104, 4, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (26, '9/22/2022', 22, 9, 2022, 3, 22, 46, 4, 'Hiver');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (27, '2/5/2022', 5, 2, 2022, 5, 5, 74, 1, 'Hiver');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (28, '10/23/2022', 23, 10, 2022, 1, 23, 324, 1, 'Hiver');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (29, '6/4/2022', 4, 6, 2022, 7, 4, 172, 2, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (30, '10/17/2022', 17, 10, 2022, 5, 17, 340, 4, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (31, '5/16/2022', 16, 5, 2022, 1, 16, 188, 2, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (32, '12/17/2021', 17, 12, 2021, 5, 17, 212, 1, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (33, '2/10/2022', 10, 2, 2022, 2, 10, 356, 3, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (34, '9/17/2022', 17, 9, 2022, 3, 17, 127, 2, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (35, '8/23/2022', 23, 8, 2022, 7, 23, 94, 1, 'Hiver');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (36, '11/26/2021', 26, 11, 2021, 7, 26, 337, 3, 'Noel');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (37, '8/4/2022', 4, 8, 2022, 6, 4, 22, 3, 'Toussaint');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (38, '6/4/2022', 4, 6, 2022, 3, 4, 192, 2, 'Hiver');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (39, '7/5/2022', 5, 7, 2022, 2, 5, 223, 4, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (40, '8/9/2022', 9, 8, 2022, 5, 9, 92, 1, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (41, '2/22/2022', 22, 2, 2022, 1, 22, 253, 1, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (42, '11/2/2022', 2, 11, 2022, 5, 2, 65, 2, 'Noel');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (43, '10/18/2022', 18, 10, 2022, 5, 18, 165, 4, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (44, '12/8/2021', 8, 12, 2021, 7, 8, 44, 4, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (45, '10/24/2022', 24, 10, 2022, 4, 24, 312, 2, 'Hiver');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (46, '10/25/2022', 25, 10, 2022, 1, 25, 193, 4, 'Ete');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (47, '11/12/2022', 12, 11, 2022, 6, 12, 341, 2, 'Hiver');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (48, '6/9/2022', 9, 6, 2022, 5, 9, 222, 4, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (49, '9/9/2022', 9, 9, 2022, 6, 9, 310, 4, 'Printemps');
insert into Dates (idDate, dates, jour, mois, annee, numJourSemaine, numJourMois, numJourAnnee, numeroTrimestre, vacances) values (50, '4/12/2022', 12, 4, 2022, 5, 12, 360, 4, 'Ete');
