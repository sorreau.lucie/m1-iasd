from tkinter import *
import tkinter as tk 



class App(tk.Tk):
 
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)
 
        self.frames = {}
        for F in (CreateApt, Evolution):
            page_name = F.__name__
            frame = F(parent=container, controller=self, app=self)
            self.frames[page_name] = frame
 

            frame.grid(row=0, column=0, sticky="nsew")
 
        self.show_frame("CreateApt")
 
    def show_frame(self, page_name):
        frame = self.frames[page_name]
        frame.tkraise()
        


 
class CreateApt(tk.Frame):

    text_var = []
    entries = []   
        # Fonction pour récupérer la matrice saisis
    def getApt(self):
        matrix = []

        rows, cols = (3,3)
        for i in range(rows):
            matrix.append([])
            for j in range(cols):
                matrix[i].append(self.text_var[i][j].get())
        print(matrix)
        return '\n'.join(' '.join(str(column) for column in row) for row in matrix)

    def updateApt(self):
        matrix = []

        rows, cols = (3,3)
        for i in range(rows):
            matrix.append([])
            for j in range(cols):
                matrix[i].append(self.text_var[i][j].get())
        return '\n'.join(' '.join(str(column) for column in row) for row in matrix)

    
    

    
    def __init__(self, parent, controller, app):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.app = app
        label = tk.Label(self, text="Créer vôtre appartement : \n - les x représentent des objet Prédefinis ( canapé, table...) \n - Les @1 représentent des portes ouvertes \n -Les @0 représentent des portes fermés \n - Les $1 représentent les fenêtres ouvertes \n -Les $0 représentent les fenêtres fermées\n - Pour réprésenter un emplacement vide il suffit de laisser la case vide", font=('arial', 10, 'bold'), bg="bisque2").place(x=20, y=20)
        x2 = 0
        y2 = 0
        rows, cols = (3, 3)
        for i in range(rows):
            # création d'une liste vide
            self.text_var.append([])
            self.entries.append([])
            for j in range(cols):
                # append des entrée
                self.text_var[i].append(StringVar())
                self.entries[i].append(Entry(self, textvariable=self.text_var[i][j], width=3))
                self.entries[i][j].place(x=150 + x2, y=150 + y2)
                x2 += 30
            y2 += 30
            x2 = 0
                
        button = tk.Button(self, text="valider",command=lambda: [controller.show_frame("Evolution"), self.app.frames["Evolution"].updateLabel(self.getApt())])
       
        button.pack()

 
 
class Evolution(tk.Frame):

    def __init__(self, parent, controller, app):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.app = app
        
        self.label = tk.Label(self, text="", font=('arial', 10, 'bold'), bg="bisque2")
        self.label.place(x=20, y=20)
        
        button = tk.Button(self, text="Etape suivant",command=lambda: [controller.show_frame("Evolution"), self.app.frames["Evolution"].updateLabel(self.app.show_frame("Evolution"))])

        button.pack()
 

    def updateLabel(self, new_text):
        self.label.configure(text=new_text)

    
        
        # a implémenter
    def closefenetre(value):
        return value
        


 
 #main
 
if __name__ == "__main__":
    app = App()
    app.geometry("650x500+120+120")
    app.configure(bg='bisque2')
    app.mainloop()

