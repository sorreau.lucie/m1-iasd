
import time
from tkinter import *


window = Tk()
window.title("Robot Sécurité")
window.geometry("650x500+120+120")
window.configure(bg='bisque2')
window.resizable(True, True)

#Tableau vide et entrée vide
text_var = []
entries = []

# Fonction pour récupérer la matrice saisis
def getApt():
    matrix = []
    for i in range(rows):
        matrix.append([])
        for j in range(cols):
            matrix[i].append(text_var[i][j].get())

    print(matrix)

Label(window, text="Créer vôtre appartement : \n - les x représentent des objet Prédefinis ( canapé, table...) \n - Les @ représentent des portes \n - Les $ représentent les fenêtre \n - les _ correspondent a un emplacement vide", font=('arial', 10, 'bold'), 
      bg="bisque2").place(x=20, y=20)

x2 = 0
y2 = 0
rows, cols = (3,3)
for i in range(rows):
    # création d'une liste vide
    text_var.append([])
    entries.append([])
    for j in range(cols):
        # append des entrée
        text_var[i].append(StringVar())
        entries[i].append(Entry(window, textvariable=text_var[i][j],width=3))
        entries[i][j].place(x=150 + x2, y=150 + y2)

        x2 += 30

    y2 += 30
    x2 = 0
button= Button(window,text="Submit", bg='bisque3', width=15, command=lambda:[getApt(), window.destroy])
button.place(x=150,y=250)


window.mainloop()


#creation une class pour les objects pour les utiliser
#ouvert= 1
#fermée= 0
class houseObjects:
    name=""
    etatOuvertureFermeture=""
    def __init__(self,objName,etatOuvertureFermeture):
        self.name=objName
        self.etatOuvertureFermeture=etatOuvertureFermeture

    #fonction pour ouvrir un port ou un fenetre
    def openObject(self):
        self.etatOuvertureFermeture=1
    
    
    #fonction pour fermer un port ou un fenetre
    def closeObject(self):
        self.etatOuvertureFermeture=0
        
    
    # just un print
    def printObject(self):
        print("""
        nom={}
        etat={}
        """.format(self.name,self.etatOuvertureFermeture))

    # return l'etat   peut etre utile apres  just un getter
    def checkCle(self):
        return self.etatOuvertureFermeture
    # check l'etat et si c'est ouvert alors il le ferme
    def checkAndClose(self):
        if(self.etatOuvertureFermeture==1):
            self.etatOuvertureFermeture=0


# un dinguerie si l'objet n'est pas connu 
def appelPolice():
    print("intruder alert")


#les nom des objets  sont bien sûre a changer 

#creation des mes object avec leur nom et leur etat ouverture et fermeture
Obj1 = houseObjects("fenetreCuisine",1)      
Obj2 = houseObjects("fenetreSalon",0)
Obj3 = houseObjects("port toilette",1)
Obj4 = houseObjects("fenetre toilette",1)





# il fait le tour de l'appartement les'_' veut dire que  il y a rien
#'x' veut dire que il y a un objet predefini dans mon plan( canape, frigo, lit de chien...)
# les $ et les @ sont utilisé pour les fenetres et des portes pour l'instant
# ce n'est pas un bon moyen il faut l'ameliorer, ceci est just vite fait pour un peut visualiser
#a chaque fenetre ou port il fait le check si il est ouvert alors il le ferme 
# à faire dans un loop infinite 
#si il y a un objet inconnu alors il appel la police
def checkApt(A):    
    for i in A:
        for j in i:
            if(j!="_" and j!="x"):
                if(j=="$1"):
                    Obj1.checkAndClose()
                elif(j=="$2"):
                    Obj2.checkAndClose()
                elif(j=="$3"):
                    Obj4.checkAndClose()
                elif(j=="@1"):
                    Obj3.checkAndClose()
                
    


#just pour faire un tour apt sans action 
def checkEtat(A):
    Safety=True
    for i in A:
        for j in i:
            if(j!="_" and j!="x"):
                if(j=="$1"):
                    Obj1.printObject()
                    if(Obj1.etatOuvertureFermeture==1):
                        Safety=False
                elif(j=="$2"):
                    Obj2.printObject()
                    if(Obj2.etatOuvertureFermeture==1):
                        Safety=False
                elif(j=="$3"):
                    Obj4.printObject()
                    if(Obj4.etatOuvertureFermeture==1):
                        Safety=False
                elif(j=="@1"):
                    Obj3.printObject()
                    if(Obj3.etatOuvertureFermeture==1):
                        Safety=False
                else:
                    print("Non secure, Object inconnue est detecté")
                    return appelPolice()
                    
    if Safety==True:
        print('Secure')
    else:
        print('Non Secure')            

    
                 
                
#fonction pour afficher mon apt 
def printApt(A):
    for i in A:
        print()
        for j in i:
            print(j," ",end="")


getApt.printObject()

