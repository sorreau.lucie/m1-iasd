import choco.Choco;
import choco.cp.model.CPModel;
import choco.cp.solver.CPSolver;
import choco.kernel.model.Model;
import choco.kernel.model.variables.integer.IntegerVariable;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.constraints.extension.Tuples;

import choco.kernel.solver.Solver;

public class Sudoku {
    public static void main(String[] args) {


        //Declaration du mod�le
        Model model = new Model("Sudoku");
        
        //Taille du probl�me
        int nbVar = 9;
        
        
        
       IntVar [] C = model.intVarArray("C",nbVar,1,9);
       int i =0;
       int j = 0;
       // check lignes 
       for( i = 0; i < nbVar; i ++) {
           for (j=0; j<nbVar;j++) {
               
               model.allDifferent(C).post();
               
           }
       }
       
       //check colonne
       for(j=0; j<nbVar; j++) {
           for() {
               
           }
       }
       
       
        
        // Affichage du réseau de contraintes créé
     System.out.println("*** Réseau Initial ***");
     System.out.println(model);
     

     // Calcul de la première solution
     if(model.getSolver().solve()) {
         System.out.println("\n\n*** Première solution ***");        
         System.out.println(model);
     }
     
             
     // Calcul de toutes les solutions
     System.out.println("\n\n*** Autres solutions ***");        
     while(model.getSolver().solve()) {      
         System.out.println("Sol "+ model.getSolver().getSolutionCount()+"\n"+model);
     }
     
     
     // Affichage de l'ensemble des caractéristiques de résolution
     System.out.println("\n\n*** Bilan ***");        
     model.getSolver().printStatistics();
        
        
    }
}