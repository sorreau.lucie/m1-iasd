package dictionary;

abstract class AbstractDictionary implements Idictionary{
	
	Object[] keys;
	Object[] values;
	 
	 protected abstract int indexOf(Object key); 
	 protected abstract int newIndexOf(Object key);
	 protected abstract int realSize();
	
	
	public  Object get(Object key){
		int i = 0;
		while( i < keys.length && keys[i] != key) i++;
		if(keys[i] == key)return values[i];
		return null;
	};
	
	
	public Idictionary put(Object key, Object value) {
		int i = this.indexOf(key);
		if (i < 0) {
			int newIndex = this.newIndexOf(key);
			keys[newIndex] = key;
			values[newIndex] = value;
			return this;
		}else {
			keys[i] = key;
			values[i] = value;	
			return this;
		}
	};
	
	public boolean isEmpty() {
		if(keys.length ==  0) {
			return true;
		}
		else {
			return false;
		}
		
	};
	public  boolean containKey(Object key) {
		if(this.indexOf(key) >= 0) {
			return true;
		}else {
			return false;
		}
		
	};
	public int size() {
		return this.realSize();		
	};
			
}


