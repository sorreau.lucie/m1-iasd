package dictionary;

public class useDictionary {

	public static void main(String[] args) {
		Idictionary dic = new OrderedDictionary();
		
		
		dic.put("key1","value1");
		dic.put("key2","value2");
		dic.put("key3","value3");

		System.out.print(dic.get("key2")+ "\n");
		
		System.out.print(dic.containKey("key5") + "\n");
		
		System.out.print(dic.isEmpty() + "\n");
		System.out.print(dic.size() + "\n");
		
		
		
	}

}
