package dictionary;

public class FastDictionary extends AbstractDictionary {

	public FastDictionary() {
		keys = new Object[4];
		values = new Object[4];
	}
	
	@Override
	public int indexOf(Object key) {
		int indiceCur = (key.hashCode() % keys.length);
		if(indiceCur < 0) indiceCur += keys.length;
		for (int i = indiceCur; i < keys.length ; i++) {
			if(keys[i] == null) {
				return -1;
			}
			if(keys[i].equals(key)) {
				return i+1;
			}
		}
		return -1;
	}

	@Override
	public int newIndexOf(Object key) {
		if (mustGrow()) grow();
		int hash = (key.hashCode() % keys.length);
		if(hash < 0) hash += keys.length;
		for (int i = hash; i < keys.length ; i++) {
			if(keys[i] == null) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public int size() {
		int res = 0;
		for(Object elt : keys) {
			if(elt != null) res++;
		}
		return res;
	}
	
	public boolean mustGrow() {
		return ((float)size() / (float)keys.length >= 0.75);
	}
	
	public Idictionary grow() {
		int newSize = keys.length * 2;
		Object[] tmpKey = new Object[newSize];
		Object[] tmpValue = new Object[newSize];
		
		for(int i = 0; i < keys.length ; i++) {
			if(this.keys[i] == null) continue;
			Object key = this.keys[i];
			int newIndex = key.hashCode() % newSize;
			if(newIndex < 0) newIndex += newSize;
			while(tmpKey[newIndex] != null) {
				newIndex++;
				if(newIndex >= newSize) newIndex = 0;
			}
			
			tmpKey[newIndex] = this.keys[i];
			tmpValue[newIndex] = this.values[i];
		}

		this.keys = tmpKey;
		this.values = tmpValue;
		
		return this;
	}

	@Override
	protected int realSize() {
		// TODO Auto-generated method stub
		return 0;
	}
}
