package dictionary;

public class OrderedDictionary extends AbstractDictionary {
	
	public OrderedDictionary() {
		this.keys = new Object[] {};
		this.values = new Object[] {};
	}
	

	@Override
	protected int indexOf(Object key) { // trouve la place ou inserer la nouvel clé et renvoie l'emplacement
		for(int i = 0; i<keys.length; i++) {
			if(keys[i].equals(key)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	protected int newIndexOf(Object key) { // recrée un tableau de taille +1 pour pouvoir inserer et renvoie l'indice la ou on peut inserer
		Object[] newKeys  = new Object[keys.length +1];
		Object[] newValues  = new Object[values.length +1];
		for(int i = 0; i < keys.length; i++) {
			newKeys[i] = keys[i];
			newValues[i] = values[i];
		}
		keys = newKeys;
		values = newValues;
		
		return (keys.length - 1);
		
	}

	protected int realSize() {
		return keys.length;
	}

}
