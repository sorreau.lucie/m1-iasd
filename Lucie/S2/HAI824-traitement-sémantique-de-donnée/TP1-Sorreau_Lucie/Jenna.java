import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.*;

public class RDFXMLGenerator {
    public static void main(String[] args) {
        Model model = ModelFactory.createDefaultModel();
        
        // Définir les espaces de noms
        String nsRDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
        String nsM = "https://dbpedia.org/page/Symphony_No._41_(Mozart)";
        String nsFOAF = "http://xmlns.com/foaf/0.1/";
        String nsDate = "https://www.w3schools.com/xml/schema_dtypes_date.asp";
        model.setNsPrefix("rdf", nsRDF);
        model.setNsPrefix("m", nsM);
        model.setNsPrefix("foaf", nsFOAF);
        model.setNsPrefix("date", nsDate);
        
        // Ajouter les ressources
        Resource symphony = model.createResource(nsM + "Symphony");
        symphony.addProperty(RDF.type, model.createResource(nsM + "Symphony"))
                .addProperty(RDFS.label, "Symphonie n41 Jupiter", "fr")
                .addProperty(model.createProperty(nsFOAF + "name"), "Symphonie n41 Jupiter", "fr");
        
        Resource composer = model.createResource(nsM + "Composer");
        composer.addProperty(RDF.type, model.createResource(nsM + "Composer"))
                .addProperty(model.createProperty(nsFOAF + "name"), "Wolfgang Amadeus Mozart", "fr");
        symphony.addProperty(model.createProperty(nsM + "composer"), composer);
        symphony.addProperty(model.createProperty(nsM + "composer"), performance);
        
        symphony.addProperty(model.createProperty(nsFOAF + "name"), "Ut majeur", "fr");
        
        Resource performance = model.createResource(nsM + "Performance");
        performance.addProperty(RDF.type, model.createResource(nsM + "Performance"))
                  .addLiteral(model.createProperty(nsDate + "year"), 1980);
        
        Resource orchestra = model.createResource(nsM + "Orchestra");
        orchestra.addProperty(RDF.type, model.createResource(nsM + "Orchestra"))
                 .addProperty(model.createProperty(nsFOAF + "name"), "Orchestre Symphonique de Londres", "fr");
        
        Resource conductor = model.createResource(nsM + "Conductor");
        conductor.addProperty(RDF.type, model.createResource(nsM + "Conductor"))
                 .addProperty(model.createProperty(nsFOAF +"name"), "claudio abbado","fr");

        Resource section1 = model.createResource(nsM + "Section");
        section1.addProperty(RDF.type, model.createResource(nsM + "Section"))
               .addProperty(model.createProperty(nsM +"keyNames"),"Ut majeur", "fr")
               .addProperty(model.createProperty(nsM +"marks"),"Allegro Vivace", "fr");

        Resource section2 = model.createResource(nsM + "Section");
        section2.addProperty(RDF.type, model.createResource(nsM + "Section"))
               .addProperty(model.createProperty(nsM +"marks"),"Antante Catabile", "fr");

        Resource section3 = model.createResource(nsM + "Section");
        section2.addProperty(RDF.type, model.createResource(nsM + "Section"))
                .addProperty(model.createProperty(nsM +"marks"),"Molto allegro", "fr");

    
        model.write(System.out, "RDF/XML-ABBREV");
    }
}