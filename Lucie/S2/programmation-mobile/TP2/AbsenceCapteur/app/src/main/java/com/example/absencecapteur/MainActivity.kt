package com.example.absencecapteur


import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var statusText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        statusText = findViewById(R.id.status_text)

        val checkButton: Button = findViewById(R.id.check_button)
        checkButton.setOnClickListener { checkSensor() }

        val exitButton: Button = findViewById(R.id.exit_button)
        exitButton.setOnClickListener { finish() }
    }

    private fun checkSensor() {
        val sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        val sensor: Sensor? = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY)

        if (sensor != null) {
            // Le capteur est disponible, effectuez l'action souhaitée ici
            statusText.text = "Le capteur est disponible"
        } else {
            // Le capteur est indisponible, informez l'utilisateur ici
            statusText.text = "Le capteur est indisponible"
        }
    }

}
