package com.example.geolocalisation
import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat

class MainActivity : AppCompatActivity() {

    private lateinit var textViewLatitude: TextView
    private lateinit var textViewLongitude: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textViewLatitude = findViewById(R.id.textViewLatitude)
        textViewLongitude = findViewById(R.id.textViewLongitude)

        // Vérifier et demander la permission d'accéder à la géolocalisation
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
        }

        // Créer une instance de LocationManager pour accéder à la géolocalisation
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        // Créer un écouteur pour la mise à jour de la géolocalisation
        val locationListener = object : LocationListener {
            override fun onLocationChanged(location: Location) {
                // Récupérer les coordonnées géographiques de l'utilisateur
                val latitude = location.latitude
                val longitude = location.longitude

                // Mettre à jour l'interface utilisateur avec les nouvelles coordonnées
                textViewLatitude.text = "Latitude : $latitude"
                textViewLongitude.text = "Longitude : $longitude"
            }

            override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}

            override fun onProviderEnabled(provider: String) {}

            override fun onProviderDisabled(provider: String) {}
        }

        // Demander les mises à jour de la géolocalisation à l'instance de LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, locationListener)
    }
}
