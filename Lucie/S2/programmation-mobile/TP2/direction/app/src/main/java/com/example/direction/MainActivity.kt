package com.example.direction

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity(), SensorEventListener {

    private lateinit var sensorManager: SensorManager
    private var accelerometer: Sensor? = null
    private var lastCoordinates: Coordinates? = null
    private lateinit var directionText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        directionText = findViewById(R.id.direction_text)

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
    }

    override fun onResume() {
        super.onResume()
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (event?.sensor?.type == Sensor.TYPE_ACCELEROMETER) {
            val x = event.values[0]
            val y = event.values[1]
            val z = event.values[2]

            lastCoordinates = Coordinates(x, y, z)
            updateDirection()
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        // Ne rien faire
    }

    private fun updateDirection() {
        if (lastCoordinates != null) {
            val x = lastCoordinates!!.x
            val y = lastCoordinates!!.y
            val z = lastCoordinates!!.z

            if (Math.abs(x) > Math.abs(y)) {
                if (x > 0) {
                    directionText.text = "Droite"
                } else {
                    directionText.text = "Gauche"
                }
            } else {
                if (y > 0) {
                    directionText.text = "Bas"
                } else {
                    directionText.text = "Haut"
                }
            }
        }
    }

    internal class Coordinates(val x: Float, val y: Float, val z: Float)
}
