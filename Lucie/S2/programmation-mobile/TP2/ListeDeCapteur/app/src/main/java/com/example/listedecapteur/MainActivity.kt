package com.example.listedecapteur

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class MainActivity : AppCompatActivity() {

    private lateinit var sensorManager: SensorManager
    private lateinit var sensorListView: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sensorListView = findViewById<ListView>(R.id.sensor_list_view)

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        val sensorList: List<Sensor> = sensorManager.getSensorList(Sensor.TYPE_ALL)

        val sensorNames = mutableListOf<String>()
        for (sensor in sensorList) {
            sensorNames.add(sensor.name)
        }

        val adapter = ArrayAdapter<String>(this,
            android.R.layout.simple_list_item_1, sensorNames)

        sensorListView.adapter = adapter
    }
}
