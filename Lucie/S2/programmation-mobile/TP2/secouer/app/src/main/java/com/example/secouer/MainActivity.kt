package com.example.secouer
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import android.hardware.camera2.CameraManager


class MainActivity : AppCompatActivity(), SensorEventListener {
    private lateinit var sensorManager: SensorManager
    private var sensor: Sensor? = null
    private var lastUpdate: Long = 0
    private var isFlashOn = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Vérifiez si l'appareil dispose d'un flash LED
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            Toast.makeText(this, "Votre appareil ne prend pas en charge le flash", Toast.LENGTH_SHORT).show()
            finish()
        }

        // Obtenez le gestionnaire de capteurs et le capteur d'accéléromètre
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
    }

    override fun onResume() {
        super.onResume()
        // Enregistrez l'écouteur de capteur lorsque l'application est active
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        // Désenregistrez l'écouteur de capteur lorsque l'application est en pause
        sensorManager.unregisterListener(this)
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        // Ne pas faire quelque chose ici
    }

    override fun onSensorChanged(event: SensorEvent?) {
        // Vérifiez si le capteur est l'accéléromètre
        if (event?.sensor?.type == Sensor.TYPE_ACCELEROMETER) {
            // Obtenez les valeurs de l'axe x, y, z
            val x = event.values[0]
            val y = event.values[1]
            val z = event.values[2]

            // Calculez la force d'accélération
            val acceleration = Math.sqrt((x * x + y * y + z * z).toDouble()).toFloat()

            // Obtenez le temps écoulé depuis la dernière mise à jour
            val now = System.currentTimeMillis()
            val timeDiff = now - lastUpdate

            // Définissez une limite de temps pour éviter les déclenchements de flash trop fréquents
            if (timeDiff > 1000) {
                lastUpdate = now

                // Vérifiez si l'accélération dépasse le seuil (vous pouvez ajuster le seuil ici)
                if (acceleration > 10) {
                    toggleFlash()
                }
            }
        }
    }

    private fun toggleFlash() {
        val cameraManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            // Obtenez l'ID de la caméra arrière
            val cameraId = cameraManager.cameraIdList[0]

            // Allumez ou éteignez le flash
            cameraManager.setTorchMode(cameraId, !isFlashOn)
            isFlashOn = !isFlashOn
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
