#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/time.h> 
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    int max_size = 1000;
    int nbsommet = 0;
    char *ligne = NULL;
    ssize_t read;
    ssize_t len = 0;
    FILE *fp;
    if (argc != 3)
    {
        printf("utilisation : %s   NuméroPort\n", argv[0]);
        exit(1);
    }

    FILE *fichier = fopen(argv[1], "r");
    if (fichier == NULL)
    {
        perror("echec a la lecture du graphe");
    }

    // On va ignorer les ligne avant les informations
    while (((read = getline(&ligne, &len, fichier)) != -1) && (ligne[0] != 'p'))
    {
    }
    char buflecture[50];
    sscanf(ligne, "p %[^ ] %i", (char *)&buflecture, &nbsommet);
    char cmd[100]="";

    char cmdC[100]="";
    strcat(cmdC,"./client 127.0.0.1 ");
    strcat(cmdC,argv[2]);
    printf("cmdC : %s \n", cmdC);
    for (int i = 0; i < nbsommet; i++)
    {
        fp = popen(cmdC,"r");
        printf("Lancement du Processus : %i \n", i);
        if(fp == NULL){
            perror("Problème de lancement du client\n");
            exit(1);
        }
        //sleep(0.2);
    }

    pclose(fp);
    return 0;
}