package dico;

public abstract class AbstractDictionary implements IDictionary{
	
	
	Object[] keys;
	Object[] values;
	private int size=0;
	
	
	protected abstract int indexof(Object key);
	// rend l’index auquel est rang ́e le nom key dans le dictionnaire receveur, si key n’est pas dans le receveur,rend -1
	
	protected abstract int newindexof(Object key);
	// Cette m ́ethode est appel ́ee uniquement si key N’EST PAS dans le dictionnaire. Cette m ́ethode pr ́epare
	// l’insertion et rend l’index auquel la nouvelle cl ́e, et la valeur correspondante, pourront ˆetre ins ́er ́ees ;
	// elle n’effectue ces insertions.
	// S’il n’y a pas assez de place pour l’insertion dans le dictionnaire concern ́e, cette m ́ethode devra se
	// charger d’en faire, en rempla ̧cant les tableaux par d’autres plus grands, afin de rendre l’insertion
	// possible.
	
	
	public  Object get(Object key) {
		int i =0;
		while (keys.length >i && keys[i]!=key) i++;
		if (keys[i]==key)return values[i];
		return null;
	}
	
	public IDictionary put(Object key , Object value) {
		int i = indexof(key);
		if ( i != -1)
			i =this.newindexof(key);
			
		this.keys[i]=key;
		this.values[i]= value;
		return this;
	} 
	 	
	 	
	
	public  boolean isEmpty() {
		return this.keys[0]==null;
	}
	
	public  boolean containkey(Object key) {
		int	i = indexof(key);
		return (i!=-1);
		
	}
	
	public  int size() {
		return this.keys.length;
	}
	
	

}
 