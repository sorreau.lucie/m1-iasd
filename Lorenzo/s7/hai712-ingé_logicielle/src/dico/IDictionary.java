package dico;

public interface IDictionary {
	
	 Object get (Object key);
	 
	 IDictionary put(Object key , Object value);
	 
	 boolean isEmpty();
	 
	 boolean containkey(Object key);
	 
	 int size();
	 
}
