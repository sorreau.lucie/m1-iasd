prompt—–exo3.1—–;
Select id_Produit,sum(Montant_Journalier) from ventes_monoprix Group by id_Produit; prompt—
–exo3.1 avec Rollup—–;
Select id_Produit,sum(Montant_Journalier) from ventes_monoprix Group by Rollup(id_Produit);
prompt—–exo3.1 avec Cube—–;
Select id_Produit,sum(Montant_Journalier) from ventes_monoprix Group by Cube(id_Produit);
prompt—–exo3.2 —–;
Select id_produit,sum(montant_Journalier) from ventes_monoprix Group by id_produit,id_ville;
2
prompt—–exo3.2 avec Rollup—–;
Select id_produit,sum(montant_Journalier) from ventes_monoprix Group by Rollup(id_produit,id_ville);
prompt—–exo3.2 avec Cube—–;
Select id_produit,sum(montant_Journalier) from ventes_monoprix Group by Cube(id_produit,id_ville);
prompt—–exo3.3—–;
Select id_produit,sum(montant_Journalier) from ventes_monoprix Group by id_produit,id_date;
prompt—–exo3.3 avec Rollup—–;
Select id_produit,sum(montant_Journalier) from ventes_monoprix Group by Rollup(id_produit,id_date);
prompt—–exo3.3 avec Cube—–;
Select id_produit,sum(montant_Journalier) from ventes_monoprix Group by Cube(id_produit,id_date);
prompt—–exo3.4—–;
Select id_magasin,id_ville,AVG(montant_Journalier) from ventes_monoprix Group by id_magasin,id_ville;
prompt—–exo3.4 avec Rollup—–;
Select id_magasin,id_ville,AVG(montant_Journalier) from ventes_monoprix Group by Rollup(id_magasin,id_ville);
prompt—–exo3.4 avec Cube—–;
Select id_magasin,id_ville,AVG(montant_Journalier) from ventes_monoprix Group by Cube(id_magasin,id_ville);
prompt—–exo3.5—–;
Select id_produit,id_ville,sum(montant_Journalier) from ventes_monoprix Group by id_produit,id_ville;
prompt—–exo3.5 avec Rollup—–;
Select id_produit,id_ville,sum(montant_Journalier) from ventes_monoprix Group by Rollup(id_produit,id_ville);
prompt—–exo3.5 avec Cube—–; Select id_produit,id_ville,sum(montant_Journalier) from ventes_monoprix
Group by Cube (id_produit,id_ville);
prompt—–exo3.6—–;
Select id_produit,id_ville,id_date,sum(montant_Journalier) from ventes_monoprix Group by
id_produit,id_ville,id_date; prompt—–exo3.6 avec Rollup—–;
Select id_produit,id_ville,id_date,sum(montant_Journalier) from ventes_monoprix Group by
Rollup(id_produit,id_ville,id_date); prompt—–exo3.6 avec Cube—–;
Select id_produit,id_ville,id_date,sum(montant_Journalier) from ventes_monoprix Group by
Cube(id_produit,id_ville,id_date);