package sem;

import org.apache.http.client.cache.Resource;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.VCARD;

public class tp {

	public static void main(String[] args) {
		// quelques définitions
		String personURI    = "http://somewhere/Jupiter";
		String nom    = "Jupiter";
		String alt   = "41 Symphonie en ";
		String p1 ="Allegro Vivace";
		String p2 ="Andante Cantabile";
		String p3="Menuetto";
		String p4="Molto Allegro";
		String gp ="orchestre symphonique de Londre";
		String dir = "Claudio Abbado";
		String rdate = "1980";
		

		// créer un modèle vide
		Model model = ModelFactory.createDefaultModel();

		// créer la ressource
		//   et ajouter des propriétés en cascade
		org.apache.jena.rdf.model.Resource Jupiter
		  = model.createResource(personURI)
		         .addProperty(DCTerms.title, nom)
		         .addProperty(VCARD.FN, alt)
		         .addProperty(DCTerms.creator, model.createResource()
		        		 .addProperty(VCARD.FN, "Wolfgang Amadeus Mozart"))
		        		 .addProperty(VCARD.GROUP, model.createResource()
		        				 .addProperty(DCTerms.hasPart, p1)
		        				 .addProperty(DCTerms.hasPart, p2)
		        				 .addProperty(DCTerms.hasPart, p3)
		        				 .addProperty(DCTerms.hasPart, p4))
		        		 .addProperty(VCARD.LABEL, model.createResource()
		        				 .addProperty(DCTerms.contributor,gp)
		        				 .addProperty(DCTerms.contributor,dir)
		        				 .addProperty(DCTerms.date,rdate));
		

				

		
		model.write(System.out,"Turtle");
	}

}
